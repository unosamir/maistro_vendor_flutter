import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart';
import 'package:maistro_vendor/Helper/ApplicationData.dart';
import 'package:maistro_vendor/Model/UserModel.dart';
import 'package:maistro_vendor/Utilities/Utilities.dart';
import 'package:meta/meta.dart';

class APIConstant {
  //URL
  static final String apiVersion = "api/v1/";

  static String registerUser = apiVersion + "admins/create";
  static String loginUser = apiVersion + "admins/login";
  static String verifyLogin = apiVersion + "admins/verify_login";
  static String setProfile = apiVersion + "admins/set_profile";
  static String getCategories = apiVersion + "categories";
  static String getStorageService = apiVersion + "storageService/list";
  static String addEditDeleteStorageService = apiVersion + "storageService";
  static String getMaintenanceService = apiVersion + "maintenanceService/list";
  static String addEditDeleteMaintenanceService = apiVersion + "maintenanceService";
  static String updateLocationOrderConfig = apiVersion + "locationOrderConfig/";
  static String requestNewItem = apiVersion + "itemRequests";
  static String getAddEditTransportService = apiVersion + "TransportationService";
}

class APIManger {
  static const _baseURL = "https://maistro.dev.api.unoapp.io";
  final Client _client = Client();

  static var shared = APIManger();

  Future<Response> request(
      {@required RequestType requestType,
      @required String path,
      Map<String, String> headers,
      Map<String, dynamic> params}) async {
    var finaHeaders = {"Content-Type": "application/json"};

    UserModel userModel = await getCurrentUser();
    if (userModel != null && userModel.token != null) {
      finaHeaders.addAll({"auth-token": userModel.token});
    }

    if (headers != null) {
      finaHeaders.addAll(headers);
    }
    switch (requestType) {
      case RequestType.GET:

        // if (params != null){
        //   final newURI = Uri.http(_baseURL, '/$path', params);
        //   return _client.get(newURI, headers: headers);
        // }else{
        //
        //   return _client.get("$_baseURL/$path");
          return _client.get("$_baseURL/$path",headers: finaHeaders);
        // }
      case RequestType.POST:
        return _client.post("$_baseURL/$path",
            headers: finaHeaders, body: json.encode(params));

      case RequestType.PUT:
        return _client.put("$_baseURL/$path",headers: finaHeaders,body: json.encode(params));

      case RequestType.DELETE:

        return _client.delete("$_baseURL/$path",headers: finaHeaders);
      default:
        return throw RequestTypeNotFoundException(
            "The HTTP request method is not found");
    }
  }

  showLoader(BuildContext context) {
    showProgressDialog(context);
  }

  hideLoader(BuildContext context) {
    Navigator.of(context).pop();
  }

  //common request
  void makeRequest(
      {@required BuildContext context,
      @required String endPoint,
      @required RequestType method,
      Map<String, String> headers,
      Map<String, dynamic> params,
      @required ValueChanged<Result> callback}) async {
    showLoader(context);

    try {
      final response = await request(
          requestType: method,
          path: endPoint,
          headers: headers,
          params: params);

      hideLoader(context);
      if (response.statusCode == 200 || response.statusCode == 201) {
        Map<String, dynamic> resp = jsonDecode(response.body);
        callback(Result.success(resp));
      } else {
        Map<String, dynamic> resp = jsonDecode(response.body);
        String msg = resp['msg'];

        if (msg.length > 0 && msg != "Please check required fields") {
          callback(Result.error(msg));
        } else {
          Map<String, dynamic> payload = resp['payload'];
          List<dynamic> arrDetails = payload['details'];
          String message = arrDetails[0]['message'];
          callback(Result.error(message));
        }
      }
    } catch (error) {
      callback(Result.error('Something went wrong'));
    }
  }
}

enum RequestType { GET, POST, DELETE, PUT }

class RequestTypeNotFoundException implements Exception {
  String cause;
  RequestTypeNotFoundException(this.cause);
}

class Result<T> {
  Result._();

  // factory Result.loading(T msg) = LoadingState<T>;

  factory Result.success(T value) = SuccessState<T>;

  factory Result.error(T msg) = ErrorState<T>;
}

class LoadingState<T> extends Result<T> {
  LoadingState(this.msg) : super._();
  final T msg;
}

class ErrorState<T> extends Result<T> {
  ErrorState(this.msg) : super._();
  final T msg;
}

class SuccessState<T> extends Result<T> {
  SuccessState(this.value) : super._();
  final T value;
}
