import 'dart:convert';
import 'package:maistro_vendor/Model/UserModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<UserModel> getCurrentUser() async {
  SharedPreferences sharedUser = await SharedPreferences.getInstance();
  String jsonStr = sharedUser.getString('user');
  if (jsonStr != null) {
    Map userMap = jsonDecode(jsonStr);
    return UserModel.fromJson(userMap);
  } else {
    return null;
  }
}
