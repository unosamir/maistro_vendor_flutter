import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/CustomWidget/AccountListView.dart';
import 'package:maistro_vendor/Utilities/NavigationMethods.dart';

// ignore: must_be_immutable
class ServiceList extends StatelessWidget{

  List<String> arrList = ['Maintenance Services','Storage Services', 'Transportation Services'];
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        title: Text('Services & Prices',
            style: TextStyle(
                fontSize: 18,
                color: ColorConstant.textMainColor,
                fontWeight: FontWeight.w800)),
        backgroundColor: Colors.white,
        elevation: 0,
        brightness: Brightness.light,
      ),
      body:  ListView.builder(
          primary: false,
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: arrList.length,
          itemBuilder: (context,index){
            return InkWell(
              child: AccountListView(arrList[index]),
              onTap: (){

                if (index == 0){

                  //maintenance
                  navigateToMaintenanceServiceList(context);
                }

                if (index == 1){

                  //storage
                  navigateToStorageServiceList(context);
                }

                if (index == 2){

                  //transportation
                  navigateToTransportServiceList(context);
                }

              },
            );
          }),
    );
  }
}