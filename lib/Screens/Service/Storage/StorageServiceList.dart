import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_section_table_view/flutter_section_table_view.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/CustomWidget/ButtonWidget.dart';
import 'package:maistro_vendor/CustomWidget/NoDataFoundWidget.dart';
import 'package:maistro_vendor/CustomWidget/Service/Storage/StorageServiceListWidget.dart';
import 'package:maistro_vendor/CustomWidget/ShadowIconButton.dart';
import 'package:maistro_vendor/Helper/APIManager.dart';
import 'package:maistro_vendor/Helper/ApplicationData.dart';
import 'package:maistro_vendor/Model/ServiceModel.dart';
import 'package:maistro_vendor/Model/Storage/StorageServiceModel.dart';
import 'package:maistro_vendor/Model/UserModel.dart';
import 'package:maistro_vendor/Screens/Service/Storage/AddStorageService.dart';
import 'package:maistro_vendor/Utilities/Utilities.dart';

// ignore: must_be_immutable
class StorageServiceList extends StatefulWidget {
  @override
  _StorageServiceListState createState() => _StorageServiceListState();
}

class _StorageServiceListState extends State<StorageServiceList> {
  TextEditingController orderPerDay = TextEditingController();
  List<StorageServiceModel> arrCategory = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Future.delayed(const Duration(milliseconds: 100), () {
      getStorageServices();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          title: Text('Storage Service',
              style: TextStyle(
                  fontSize: 18,
                  color: ColorConstant.textMainColor,
                  fontWeight: FontWeight.w800)),
          backgroundColor: Colors.white,
          elevation: 0,
          brightness: Brightness.light,
        ),
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Text(
                    'Add and manage your services and prices for storage.',
                    style: TextStyle(
                        fontSize: 16, color: ColorConstant.textMainColor)),
              ),
              ShadowIconButton(
                  onPressed: () {
                    List<ServiceModel> arrData = [];
                    for (final model in arrCategory) {
                      arrData.addAll(model.arrServices);
                    }
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddStorageService(
                              arrAlreadyAdded: arrData,
                              callBack: () {
                                getStorageServices();
                              }),
                          fullscreenDialog: true),
                    );
                  },
                  title: 'Add Service'),
              SizedBox(height: 10),
              arrCategory.length == 0
                  ? Spacer()
                  : Container(width: 0, height: 0),
              arrCategory.length == 0
                  ? NoDataFoundWidget('You have not added any offers')
                  : Expanded(
                      child: SectionTableView(
                        sectionCount: arrCategory.length,
                        numOfRowInSection: (section) {
                          return arrCategory[section].isSelected
                              ? arrCategory[section].arrServices.length
                              : 0;
                        },
                        cellAtIndexPath: (section, row) {
                          return StorageServiceListWidget(
                              model: arrCategory[section].arrServices[row],
                              onEditClick: () {
                                setState(() {
                                  arrCategory[section]
                                          .arrServices[row]
                                          .isSelected =
                                      !arrCategory[section]
                                          .arrServices[row]
                                          .isSelected;
                                });
                              },
                              onDeleteClick: () {
                                getStorageServices();
                              },
                              onSaveClick: () {
                                getStorageServices();
                              });
                        },
                        headerInSection: (section) {
                          return GestureDetector(
                            onTap: () {
                              setState(() {
                                arrCategory[section].isSelected =
                                    !arrCategory[section].isSelected;
                              });
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(arrCategory[section].category_name,
                                    style: TextStyle(
                                        fontSize: 24,
                                        color: ColorConstant.themeColor,
                                        fontWeight: FontWeight.bold)),
                                SizedBox(width: 10),
                                Image.asset(
                                    arrCategory[section].isSelected
                                        ? 'assets/images/upArrowLine.png'
                                        : 'assets/images/downArrowLine.png',
                                    height: 20,
                                    width: 20,
                                    color: ColorConstant.themeColor)
                              ],
                            ),
                          );
                        },
                      ),
                    ),
              arrCategory.length == 0
                  ? Spacer()
                  : Container(width: 0, height: 0),
              arrCategory.length == 0
                  ? AbsorbPointer(
                      child: Container(
                          child: bottomWidget(),
                          color: Colors.grey.withOpacity(0.2)))
                  : bottomWidget()
            ],
          ),
        ));
  }

  bottomWidget() {
    return Column(
      children: <Widget>[
        Container(
            margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
            height: 60,
            width: MediaQuery.of(context).size.width,
            child: Row(
              children: <Widget>[
                Text('Max. orders per day',
                    style: TextStyle(
                        fontSize: 16,
                        color: ColorConstant.textMainColor,
                        fontWeight: FontWeight.w500)),
                Spacer(),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                  height: 50,
                  width: 100,
                  child: TextFormField(
                      controller: orderPerDay,
                      keyboardType: TextInputType.number,
                      style: TextStyle(
                          fontSize: 14.0, color: ColorConstant.themeColor),
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: ColorConstant.disableAndTextFieldBGColor,
                          hintStyle: TextStyle(
                              fontSize: 14.0, color: ColorConstant.themeColor),
                          hintText: '0',
                          border: InputBorder.none)),
                )
              ],
            )),
        Container(
            child: ButtonWidget(
              onPressed: () {
                if (orderPerDay.text.length == 0) {
                  showToast('Please enter valid max order per day.');
                } else {
                  callAPIAddServices();
                }
              },
              title: "NEXT",
              bgColor: ColorConstant.themeColor,
              textColor: Colors.white,
              borderColor: Colors.transparent,
            ),
            height: 60,
            width: MediaQuery.of(context).size.width)
      ],
    );
  }

  getStorageServices() async {
    UserModel userModel = await getCurrentUser();
    int businessId = userModel.businessProfile.id;

    APIManger.shared.makeRequest(
        context: context,
        endPoint: APIConstant.getStorageService + '/' + '$businessId',
        method: RequestType.GET,
        params: {'status': 'active'},
        callback: (result) {
          if (result is SuccessState) {
            arrCategory.clear();

            List arrData = result.value['payload'] as List;
            List<ServiceModel> arrAllServices =
                arrData.map((item) => ServiceModel.fromJSON(item)).toList();

            //set max order per day
            if (arrAllServices.length > 0) {
              var model = arrAllServices[0];
              orderPerDay.text =
                  model.locationlocationOrderConfigmaxStorageOrders.toString();
            }

            //make category
            for (final modelService in arrAllServices) {
              var arr = arrCategory
                  .where((i) => i.category_id == modelService.itemcategoryId)
                  .toList();
              if (arr.length == 0) {
                var model = StorageServiceModel();
                model.category_id = modelService.itemcategoryId;
                model.category_name = modelService.itemcategoryname;
                arrCategory.add(model);
              }
            }

            //make service
            for (final mm in arrCategory) {
              var arrSer = arrAllServices
                  .where((element) => element.itemcategoryId == mm.category_id)
                  .toList();
              mm.arrServices.addAll(arrSer);
            }

            //sorting items
            for (final mm in arrCategory) {
              mm.arrServices.sort((element0, element1) => element0.itemname
                  .toLowerCase()
                  .compareTo(element1.itemname.toLowerCase()));
            }

            //sort categories
            arrCategory.sort((element0, element1) => element0.category_name
                .toLowerCase()
                .compareTo(element1.category_name.toLowerCase()));

            setState(() {
              arrCategory = arrCategory;
            });
          } else if (result is ErrorState) {
            showToast(result.msg);
          }
        });
  }

  callAPIAddServices() async {
    UserModel userModel = await getCurrentUser();
    int businessId = userModel.businessProfile.id;

    List<ServiceModel> arrSelected = [];

    for (final mm in arrCategory) {
      arrSelected.addAll(mm.arrServices);
    }

    Map<String, dynamic> param = {};
    List<Map<String, dynamic>> arrParam = [];
    for (final model in arrSelected) {
      Map<String, dynamic> paramMM = {};
      paramMM.addAll({'item_id': model.itemid});
      paramMM.addAll({'price': model.price ?? 0.0});
      paramMM.addAll({'is_add_on_only': model.isAddOn ? 1 : 0});
      paramMM.addAll({'status': 'active'});
      arrParam.add(paramMM);
    }

    param.addAll({'location_id': businessId});
    param.addAll({'items': arrParam});
    param.addAll({'max_storage_orders': orderPerDay.text});

    APIManger.shared.makeRequest(
        context: context,
        endPoint: APIConstant.addEditDeleteStorageService,
        method: RequestType.POST,
        params: param,
        callback: (result) {
          if (result is SuccessState) {
            showToast("Service Updated Successfully.");
            Navigator.of(context).pop();
          } else if (result is ErrorState) {
            showToast(result.msg);
          }
        });
  }
}
