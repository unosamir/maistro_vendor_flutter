import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_section_table_view/flutter_section_table_view.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/CustomWidget/ButtonWidget.dart';
import 'package:maistro_vendor/CustomWidget/Service/RequestNewItemPopupWidget.dart';
import 'package:maistro_vendor/CustomWidget/Service/Storage/AddStorageWidget.dart';
import 'package:maistro_vendor/Helper/APIManager.dart';
import 'package:maistro_vendor/Helper/ApplicationData.dart';
import 'package:maistro_vendor/Model/CommonServiceModel.dart';
import 'package:maistro_vendor/Model/ServiceModel.dart';
import 'package:maistro_vendor/Model/UserModel.dart';
import 'package:maistro_vendor/Utilities/Utilities.dart';

class AddStorageService extends StatefulWidget {
  AddStorageService({@required this.arrAlreadyAdded, @required this.callBack});

  final List<ServiceModel> arrAlreadyAdded;
  final Function callBack;
  @override
  _AddStorageServiceState createState() => _AddStorageServiceState();
}

class _AddStorageServiceState extends State<AddStorageService> {
  List<CommonServiceModel> arrCategory = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Future.delayed(const Duration(milliseconds: 100), () {
      getCategories();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        title: Text('Add Services',
            style: TextStyle(
                fontSize: 18,
                color: ColorConstant.textMainColor,
                fontWeight: FontWeight.w800)),
        backgroundColor: Colors.white,
        elevation: 0,
        brightness: Brightness.light,
      ),
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: Column(children: <Widget>[
            Text('Select items you want to provide storage service for',
                style: TextStyle(
                    fontSize: 16, color: ColorConstant.textMainColor)),
            SizedBox(height: 10),
            Expanded(
              child: SectionTableView(
                sectionCount: arrCategory.length,
                numOfRowInSection: (section) {
                  return arrCategory[section].items.length;
                },
                cellAtIndexPath: (section, row) {
                  return AddStorageWidget(arrCategory[section].items[row], () {
                    setState(() {
                      arrCategory[section].items[row].isSelected =
                          !arrCategory[section].items[row].isSelected;
                    });
                  });
                },
                headerInSection: (section) {
                  return Container(
                    alignment: Alignment.center,
                    child: Text(arrCategory[section].name,
                        style: TextStyle(
                            fontSize: 24,
                            color: ColorConstant.themeColor,
                            fontWeight: FontWeight.bold)),
                  );
                },
              ),
            ),
            Container(
                height: 50,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Text("Dont't see an item?",
                          textAlign: TextAlign.end,
                          style: TextStyle(
                              fontSize: 18,
                              color: ColorConstant.textFieldTextColor)),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {

                         showRequestNewItemPopup(context);
                        },
                        child: Text('  Request Now',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontSize: 18,
                                color: ColorConstant.themeColor,
                                fontWeight: FontWeight.bold)),
                      ),
                    )
                  ],
                )),
            Container(
                child: ButtonWidget(
                  onPressed: () {
                    List<CommonServiceModel> arrSelected = [];

                    for (final mm in arrCategory) {
                      var filter = mm.items
                          .where((element) => element.isSelected)
                          .toList();
                      arrSelected.addAll(filter);
                    }

                    if (arrSelected.length == 0) {
                      showToast('Please select atleast one servie');
                    } else {
                      callAPIAddServices(arrSelected);
                    }
                  },
                  title: "Add Services",
                  bgColor: ColorConstant.themeColor,
                  textColor: Colors.white,
                  borderColor: Colors.transparent,
                ),
                height: 60,
                width: MediaQuery.of(context).size.width)
          ]),
        ),
      ),
    );
  }

  getCategories() {
    APIManger.shared.makeRequest(
        context: context,
        endPoint: APIConstant.getCategories,
        method: RequestType.GET,
        callback: (result) {
          if (result is SuccessState) {
            var arrCategoryJSON = result.value['payload'] as List;
            List<CommonServiceModel> arrData = arrCategoryJSON
                .map((category) => CommonServiceModel.fromJson(category))
                .toList();

            //check if already added
            for (final model in arrData) {
              for (final mm in model.items) {
                var filter = widget.arrAlreadyAdded
                    .where((element) => element.itemid == mm.id)
                    .toList();
                if (filter.length > 0) {
                  mm.isAlreadyAdded = true;
                  mm.price = filter.first.price ?? 0.0;
                  mm.isAddOn = filter.first.isAddOn;
                }
              }
            }

            setState(() {
              arrCategory = arrData;
            });
          } else if (result is ErrorState) {
            showToast(result.msg);
          }
        });
  }

  callAPIAddServices(List<CommonServiceModel> arrSelected) async {
    UserModel userModel = await getCurrentUser();
    int businessId = userModel.businessProfile.id;

    Map<String, dynamic> param = {};
    List<Map<String, dynamic>> arrParam = [];
    for (final model in arrSelected) {
      Map<String, dynamic> paramMM = {};
      paramMM.addAll({'item_id': model.id});
      paramMM.addAll({'price': model.price ?? 0.0});
      paramMM.addAll({'is_add_on_only': model.isAddOn ? 1 : 0});
      paramMM.addAll({'status': 'active'});
      arrParam.add(paramMM);
    }

    param.addAll({'location_id': businessId});
    param.addAll({'items': arrParam});

    APIManger.shared.makeRequest(
        context: context,
        endPoint: APIConstant.addEditDeleteStorageService,
        method: RequestType.POST,
        params: param,
        callback: (result) {
          if (result is SuccessState) {
            showToast("Service Added Successfully.");
            widget.callBack();
            Navigator.pop(context);
          } else if (result is ErrorState) {
            showToast(result.msg);
          }
        });
  }
}
