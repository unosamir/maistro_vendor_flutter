import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/CustomWidget/ButtonWidget.dart';
import 'package:maistro_vendor/CustomWidget/Service/Maintenance/MaintenanceServiceDropDownWidget.dart';
import 'package:maistro_vendor/CustomWidget/TextFieldWidget.dart';
import 'package:maistro_vendor/Helper/APIManager.dart';
import 'package:maistro_vendor/Helper/ApplicationData.dart';
import 'package:maistro_vendor/Model/CommonServiceModel.dart';
import 'package:maistro_vendor/Model/ServiceModel.dart';
import 'package:maistro_vendor/Model/UserModel.dart';
import 'package:maistro_vendor/Utilities/Utilities.dart';

// ignore: must_be_immutable
class AddMaintenanceService extends StatefulWidget {
  AddMaintenanceService(
      {this.isFromEdit = false,
      this.serviceModel,
      this.selectedCategory,
      this.selectedItem,
      @required this.callBack});
  final Function callBack;
  bool isFromEdit;
  ServiceModel serviceModel;
  CommonServiceModel selectedCategory;
  CommonServiceModel selectedItem;

  @override
  _AddMaintenanceServiceState createState() => _AddMaintenanceServiceState();
}

class _AddMaintenanceServiceState extends State<AddMaintenanceService> {
  TextEditingController titleController = TextEditingController();
  TextEditingController descController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController daysController = TextEditingController();

  List<CommonServiceModel> arrCategory = [];
  List<CommonServiceModel> arrItem = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Future.delayed(const Duration(milliseconds: 100), () {
      getCategories();
    });

    if (widget.isFromEdit) {
      titleController.text = widget.serviceModel.title;
      descController.text = widget.serviceModel.descriptionField;
      priceController.text = widget.serviceModel.price.toString();
      daysController.text = widget.serviceModel.days_to_complete.toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        title: Text(widget.isFromEdit ? 'Edit Services' : 'Add a Services',
            style: TextStyle(
                fontSize: 18,
                color: ColorConstant.textMainColor,
                fontWeight: FontWeight.w800)),
        backgroundColor: Colors.white,
        elevation: 0,
        brightness: Brightness.light,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  child: Text('Set a name and price of your service.',
                      style: TextStyle(
                          fontSize: 16, color: ColorConstant.textMainColor)),
                ),
                SizedBox(height: 30),
                MaintenanceServiceDropDownWidget(
                    initialValueModel: widget.selectedCategory,
                    mainTitle: 'Item Category',
                    subTitle: 'Select the category',
                    dropDownTitle: 'Choose Category',
                    arrData: arrCategory,
                    selectedModel: (model) {
                      setState(() {
                        widget.selectedCategory = model;

                        //set items data
                        widget.selectedItem = null;
                        arrItem = widget.selectedCategory.items;
                      });
                    }),
                SizedBox(height: 20),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    if (widget.selectedCategory == null) {
                      showToast('Please select category first');
                    }
                  },
                  child: MaintenanceServiceDropDownWidget(
                      initialValueModel: widget.selectedItem,
                      mainTitle: 'Item Type',
                      subTitle: 'What type of item is this service for? ',
                      dropDownTitle: 'Choose Item',
                      arrData: arrItem,
                      selectedModel: (model) {
                        setState(() {
                          widget.selectedItem = model;
                        });
                      }),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 0, 0, 0),
                  child: Text('Title',
                      style: TextStyle(
                          fontSize: 16,
                          color: ColorConstant.textMainColor,
                          fontWeight: FontWeight.w500)),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                  child: TextFieldWidget(
                      controller: titleController,
                      placeHolder: 'Title',
                      isSecure: false,
                      keyboardType: TextInputType.name,
                      errorMsg: 'Please enter title',
                      textFieldType: TextFieldType.EMAIL),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 0, 0, 0),
                  child: Text('Description (optional)',
                      style: TextStyle(
                          fontSize: 16,
                          color: ColorConstant.textMainColor,
                          fontWeight: FontWeight.w500)),
                ),
                SizedBox(height: 10),
                Container(
                    height: 100,
                    padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                    child: TextField(
                      controller: descController,
                      maxLines: null,
                      expands: true,
                      keyboardType: TextInputType.multiline,
                      decoration: InputDecoration(
                          hintStyle:
                              TextStyle(fontSize: 16.0, color: Colors.grey),
                          filled: true,
                          fillColor: ColorConstant.disableAndTextFieldBGColor,
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                          hintText: 'optional description',
                          border: InputBorder.none),
                    ))
              ]),
        ),
      ),
      bottomNavigationBar: bottomWidget(),
    );
  }

  bottomWidget() {
    return SafeArea(
      child: Container(
        height: 240,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            widget.isFromEdit
                ? SizedBox(height: 0)
                : Container(
                    alignment: Alignment.center,
                    height: 50,
                    child: GestureDetector(
                      onTap: () {},
                      child: Text('Find Archived Service',
                          style: TextStyle(
                              fontSize: 18,
                              color: ColorConstant.themeColor,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
            widget.isFromEdit
                ? SizedBox(height: 0)
                : Container(height: 3, color: ColorConstant.bgColor),
            Container(
                margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                height: 60,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: <Widget>[
                    Text('Price',
                        style: TextStyle(
                            fontSize: 16,
                            color: ColorConstant.textMainColor,
                            fontWeight: FontWeight.w500)),
                    Spacer(),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                      height: 50,
                      width: 150,
                      child: TextFormField(
                          controller: priceController,
                          keyboardType: TextInputType.number,
                          style: TextStyle(
                              fontSize: 14.0, color: ColorConstant.themeColor),
                          decoration: InputDecoration(
                              filled: true,
                              fillColor:
                                  ColorConstant.disableAndTextFieldBGColor,
                              hintStyle:
                                  TextStyle(fontSize: 14.0, color: Colors.grey),
                              hintText: '0.00',
                              border: InputBorder.none)),
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                height: 60,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: <Widget>[
                    Text('Days To Complete',
                        style: TextStyle(
                            fontSize: 16,
                            color: ColorConstant.textMainColor,
                            fontWeight: FontWeight.w500)),
                    Spacer(),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                      height: 50,
                      width: 150,
                      child: TextFormField(
                          controller: daysController,
                          keyboardType: TextInputType.number,
                          style: TextStyle(
                              fontSize: 14.0, color: ColorConstant.themeColor),
                          decoration: InputDecoration(
                              filled: true,
                              fillColor:
                                  ColorConstant.disableAndTextFieldBGColor,
                              hintStyle:
                                  TextStyle(fontSize: 14.0, color: Colors.grey),
                              hintText: '0',
                              border: InputBorder.none)),
                    )
                  ],
                )),
            widget.isFromEdit
                ? Container(height: 3, color: ColorConstant.bgColor)
                : SizedBox(height: 0),
            widget.isFromEdit
                ? Container(
                    height: 50,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              showAlertDialogWithTwoAction(
                                  context,
                                  'Archive',
                                  "Are you sure you want to archive?",
                                  "No",
                                  "Yes", () {
                                callAPIFotAddEditServices(true);
                              });
                            },
                            child: Text('Archive',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 18,
                                    color: ColorConstant.themeColor,
                                    fontWeight: FontWeight.bold)),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              showAlertDialogWithTwoAction(
                                  context,
                                  'Delete',
                                  "Are you sure you want to delete?",
                                  "No",
                                  "Yes", () {
                                callAPIFotDeleteService();
                              });
                            },
                            child: Text('Delete',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 18,
                                    color: ColorConstant.textFieldErrorColor,
                                    fontWeight: FontWeight.bold)),
                          ),
                        )
                      ],
                    ))
                : SizedBox(height: 0),
            Container(
                child: ButtonWidget(
                  onPressed: () {
                    if (widget.selectedCategory == null) {
                      showToast('Please select category');
                    } else if (widget.selectedItem == null) {
                      showToast('Please select item');
                    } else if (titleController.text.isEmpty) {
                      showToast('Please enter title');
                    } else if (descController.text.isEmpty) {
                      showToast('Please enter description');
                    } else if (priceController.text.isEmpty) {
                      showToast('Please enter price');
                    } else if (daysController.text.isEmpty) {
                      showToast('Please enter days');
                    } else {
                      //api call
                      callAPIFotAddEditServices(false);
                    }
                  },
                  title: widget.isFromEdit ? "Save" : "Add Services",
                  bgColor: ColorConstant.themeColor,
                  textColor: Colors.white,
                  borderColor: Colors.transparent,
                ),
                height: 60,
                width: MediaQuery.of(context).size.width)
          ],
        ),
      ),
    );
  }

  getCategories() {
    APIManger.shared.makeRequest(
        context: context,
        endPoint: APIConstant.getCategories,
        method: RequestType.GET,
        callback: (result) {
          if (result is SuccessState) {
            var arrCategoryJSON = result.value['payload'] as List;
            List<CommonServiceModel> arrData = arrCategoryJSON
                .map((category) => CommonServiceModel.fromJson(category))
                .toList();

            setState(() {
              arrCategory = arrData;
              if (widget.isFromEdit) {
                var arr = arrCategory
                    .where((i) => i.id == widget.selectedCategory.id)
                    .toList();

                widget.selectedCategory = arr.first;

                arrItem = widget.selectedCategory.items;
                var items = widget.selectedCategory.items
                    .where((i) => i.id == widget.selectedItem.id)
                    .toList();

                widget.selectedItem = items.first;
              }
            });
          } else if (result is ErrorState) {
            showToast(result.msg);
          }
        });
  }

  callAPIFotAddEditServices(bool isArchived) async {
    UserModel userModel = await getCurrentUser();
    int businessId = userModel.businessProfile.id;

    Map<String, dynamic> param = {};

    param.addAll({'location_id': businessId});
    param.addAll({'price': double.parse(priceController.text)});
    param.addAll({'title': titleController.text});
    param.addAll({'description': descController.text});
    param.addAll({'sort': widget.selectedItem.sort});
    param.addAll({
      'item_id': widget.selectedItem.itemId == null
          ? widget.selectedItem.id
          : widget.selectedItem.itemId
    });
    param.addAll({'days_to_complete': int.parse(daysController.text)});
    if (isArchived) {
      param.addAll({'status': 'archived'});
    } else {
      param.addAll({'status': 'active'});
    }

    var method = RequestType.POST;
    String url = APIConstant.addEditDeleteMaintenanceService;
    if (widget.isFromEdit){
      method = RequestType.PUT;
      url = APIConstant.addEditDeleteMaintenanceService+'/'+widget.serviceModel.id.toString();
    }

    APIManger.shared.makeRequest(
        context: context,
        endPoint: url,
        method: method,
        params: param,
        callback: (result) {
          if (result is SuccessState) {
            String msg = result.value['msg'];
            showToast(msg);
            widget.callBack();
            Navigator.pop(context);
          } else if (result is ErrorState) {
            showToast(result.msg);
          }
        });
  }

  callAPIFotDeleteService() async {
    String url = APIConstant.addEditDeleteMaintenanceService +
        '/' +
        widget.serviceModel.id.toString();

    APIManger.shared.makeRequest(
        context: context,
        endPoint: url,
        method: RequestType.DELETE,
        callback: (result) {
          if (result is SuccessState) {
            String msg = result.value['msg'];
            showToast(msg);
            widget.callBack();
            Navigator.pop(context);
          } else if (result is ErrorState) {
            showToast(result.msg);
          }
        });
  }
}
