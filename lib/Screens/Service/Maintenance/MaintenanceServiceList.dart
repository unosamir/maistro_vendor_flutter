import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_section_table_view/flutter_section_table_view.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/CustomWidget/ButtonWidget.dart';
import 'package:maistro_vendor/CustomWidget/NoDataFoundWidget.dart';
import 'package:maistro_vendor/CustomWidget/Service/Maintenance/MaintenanceServiceListWidget.dart';
import 'package:maistro_vendor/CustomWidget/ShadowIconButton.dart';
import 'package:maistro_vendor/Helper/APIManager.dart';
import 'package:maistro_vendor/Helper/ApplicationData.dart';
import 'package:maistro_vendor/Model/CommonServiceModel.dart';
import 'package:maistro_vendor/Model/Maintenance/MaintenanceServiceModel.dart';
import 'package:maistro_vendor/Model/ServiceModel.dart';
import 'package:maistro_vendor/Model/UserModel.dart';
import 'package:maistro_vendor/Screens/Service/Maintenance/AddMaintenanceService.dart';
import 'package:maistro_vendor/Utilities/Utilities.dart';

// ignore: must_be_immutable
class MaintenanceServiceList extends StatefulWidget {
  @override
  _MaintenanceServiceListState createState() => _MaintenanceServiceListState();
}

class _MaintenanceServiceListState extends State<MaintenanceServiceList> {
  TextEditingController orderPerDay = TextEditingController();
  List<MaintenanceCategoryModel> arrCategory = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Future.delayed(const Duration(milliseconds: 100), () {
      getMaintenanceServices();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          title: Text('Maintenance Service',
              style: TextStyle(
                  fontSize: 18,
                  color: ColorConstant.textMainColor,
                  fontWeight: FontWeight.w800)),
          backgroundColor: Colors.white,
          elevation: 0,
          brightness: Brightness.light,
        ),
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Text(
                    'Add and manage your services and prices for maintenance.',
                    style: TextStyle(
                        fontSize: 16, color: ColorConstant.textMainColor)),
              ),
              ShadowIconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddMaintenanceService(
                              callBack: () {
                               getMaintenanceServices();
                              }),
                          fullscreenDialog: true),
                    );
                  },
                  title: 'Add Service'),
              SizedBox(height: 10),
              arrCategory.length == 0
                  ? Spacer()
                  : Container(width: 0, height: 0),
              arrCategory.length == 0
                  ? NoDataFoundWidget('You have not added any offers')
                  : Expanded(
                      child: SectionTableView(
                        sectionCount: arrCategory.length,
                        numOfRowInSection: (section) {
                          return arrCategory[section].arrItems.length;
                        },
                        cellAtIndexPath: (section, row) {
                          return MaintenanceServiceListWidget(model: arrCategory[section].arrItems[row],onEditTap: (serviceModel){

                            //Edit Click

                            //category model
                            var catModel = CommonServiceModel();
                            catModel.id = arrCategory[section].category_id;
                            catModel.name = arrCategory[section].category_name;

                            //item model
                            var itemMM = CommonServiceModel();
                            itemMM.id = arrCategory[section].arrItems[row].item_id;
                            itemMM.name = arrCategory[section].arrItems[row].item_name;

                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddMaintenanceService(isFromEdit: true,serviceModel: serviceModel,selectedCategory: catModel,selectedItem: itemMM,
                                      callBack: () {
                                        getMaintenanceServices();
                                      }),
                                  fullscreenDialog: true),
                            );
                          });
                        },
                        headerInSection: (section) {
                          return Container(
                              alignment: Alignment.center,
                              child: Text(arrCategory[section].category_name,
                                  style: TextStyle(
                                      fontSize: 24,
                                      color: ColorConstant.themeColor,
                                      fontWeight: FontWeight.bold)));
                        },
                      ),
                    ),
              arrCategory.length == 0
                  ? Spacer()
                  : Container(width: 0, height: 0),
              arrCategory.length == 0
                  ? AbsorbPointer(
                      child: Container(
                          child: bottomWidget(),
                          color: Colors.grey.withOpacity(0.2)))
                  : bottomWidget()
            ],
          ),
        ));
  }

  bottomWidget() {
    return Column(
      children: <Widget>[
        Container(
            margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
            height: 60,
            width: MediaQuery.of(context).size.width,
            child: Row(
              children: <Widget>[
                Text('Max. orders per day',
                    style: TextStyle(
                        fontSize: 16,
                        color: ColorConstant.textMainColor,
                        fontWeight: FontWeight.w500)),
                Spacer(),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                  height: 50,
                  width: 100,
                  child: TextFormField(
                      controller: orderPerDay,
                      keyboardType: TextInputType.number,
                      style: TextStyle(
                          fontSize: 14.0, color: ColorConstant.themeColor),
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: ColorConstant.disableAndTextFieldBGColor,
                          hintStyle: TextStyle(
                              fontSize: 14.0, color: ColorConstant.themeColor),
                          hintText: '0',
                          border: InputBorder.none)),
                )
              ],
            )),
        Container(
            child: ButtonWidget(
              onPressed: () {
                if (orderPerDay.text.length == 0) {
                  showToast('Please enter valid max order per day.');
                } else {

                  callAPIForUpdateLocationConfig();
                }
              },
              title: "NEXT",
              bgColor: ColorConstant.themeColor,
              textColor: Colors.white,
              borderColor: Colors.transparent,
            ),
            height: 60,
            width: MediaQuery.of(context).size.width)
      ],
    );
  }

  getMaintenanceServices() async {
    UserModel userModel = await getCurrentUser();
    int businessId = userModel.businessProfile.id;

    APIManger.shared.makeRequest(
        context: context,
        endPoint: APIConstant.getMaintenanceService + '/' + '$businessId',
        method: RequestType.GET,
        params: {'status': 'active'},
        callback: (result) {
          if (result is SuccessState) {
            arrCategory.clear();

            List arrData = result.value['payload'] as List;
            List<ServiceModel> arrAllServices =
                arrData.map((item) => ServiceModel.fromJSON(item)).toList();

            //set max order per day
            if (arrAllServices.length > 0) {
              var model = arrAllServices[0];
              if ( model
                  .locationlocationOrderConfigmaxMaintenanceOrders == null){

                orderPerDay.text = '0';
              }else{

                orderPerDay.text = model
                    .locationlocationOrderConfigmaxMaintenanceOrders
                    .toString();
              }
            }

            //make category
            for (final modelService in arrAllServices) {
              var arr = arrCategory
                  .where((i) => i.category_id == modelService.itemcategoryId)
                  .toList();
              if (arr.length == 0) {
                var model = MaintenanceCategoryModel();
                model.category_id = modelService.itemcategoryId;
                model.category_name = modelService.itemcategoryname;
                arrCategory.add(model);
              }
            }

            //make items
            for (final modelService in arrCategory) {
              var arr = arrAllServices
                  .where((i) => i.itemcategoryId == modelService.category_id)
                  .toList();
              for (final item in arr) {
                var arrInner = modelService.arrItems
                    .where((i) => i.item_id == item.itemId)
                    .toList();

                if (arrInner.length == 0) {
                  var model = MaintenanceItemModel();
                  model.item_id = item.itemId;
                  model.item_name = item.itemname;
                  modelService.arrItems.add(model);
                }
              }
            }

            //make service
            for (final mm in arrCategory) {
              for (final item in mm.arrItems) {
                var arrSer = arrAllServices
                    .where((element) => element.itemId == item.item_id)
                    .toList();
                item.arrService.addAll(arrSer);
              }
            }

            //sorting items
            for (final mm in arrCategory) {
              mm.arrItems.sort((element0, element1) => element0.item_name
                  .toLowerCase()
                  .compareTo(element1.item_name.toLowerCase()));
            }

            //sort categories
            arrCategory.sort((element0, element1) => element0.category_name
                .toLowerCase()
                .compareTo(element1.category_name.toLowerCase()));

            setState(() {
              arrCategory = arrCategory;
            });
          } else if (result is ErrorState) {
            showToast(result.msg);
          }
        });
  }

  callAPIForUpdateLocationConfig() async {
    UserModel userModel = await getCurrentUser();
    int businessId = userModel.businessProfile.id;

    Map<String, dynamic> param = {};
    param.addAll({'max_maintenance_orders': orderPerDay.text});

    String url = APIConstant.updateLocationOrderConfig + businessId.toString() + '/update_by_location';

    APIManger.shared.makeRequest(
        context: context,
        endPoint: url,
        method: RequestType.PUT,
        params: param,
        callback: (result) {
          if (result is SuccessState) {
            showToast("Location order configuration updated successfully");
            Navigator.of(context).pop();
          } else if (result is ErrorState) {
            showToast(result.msg);
          }
        });
  }
}
