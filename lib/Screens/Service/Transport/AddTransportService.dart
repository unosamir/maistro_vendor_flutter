import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/Constant/FontConstant.dart';
import 'package:maistro_vendor/CustomWidget/ButtonWidget.dart';
import 'package:maistro_vendor/Helper/APIManager.dart';
import 'package:maistro_vendor/Helper/ApplicationData.dart';
import 'package:maistro_vendor/Model/UserModel.dart';
import 'package:maistro_vendor/Utilities/Utilities.dart';

class AddTransportService extends StatefulWidget {
  @override
  _AddTransportServiceState createState() => _AddTransportServiceState();
}

class _AddTransportServiceState extends State<AddTransportService> {
  double serviceRadius = 0;
  double extendedServiceRadius = 0;
  double extendedServiceMinimum = 0;

  TextEditingController pricePerKmController = TextEditingController();
  TextEditingController kmOverController = TextEditingController();
  TextEditingController tripsController = TextEditingController();
  TextEditingController transportController = TextEditingController();

  bool isExtendedService = false;
  bool isAddon = false;
  int serviceId = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Future.delayed(const Duration(milliseconds: 100), () {
      getTransportService();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CupertinoPageScaffold(
      child: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            CupertinoSliverNavigationBar(
              actionsForegroundColor: Colors.black,
              border: Border.all(color: Colors.transparent),
              backgroundColor: Colors.white,
              brightness: Brightness.light,
              largeTitle: Text('Transportation',
                  style: TextStyle(
                      fontFamily: FontConstant.kFontFamily,
                      fontSize: 22,
                      color: ColorConstant.textMainColor,
                      fontWeight: FontWeight.w800)),
            )
          ];
        },
        body: Material(
            color: Colors.white,
            child: SafeArea(
              child: Column(children: <Widget>[
                Expanded(child: Padding(
                  padding: EdgeInsets.fromLTRB(16, 0, 16, 16),
                  child: ListView(primary: true, children: <Widget>[
                    Text('Please add transportation price details.',
                        style: TextStyle(
                            fontSize: 16, color: ColorConstant.textMainColor)),
                    SizedBox(height: 30),
                    Text('Regular Service Radius',
                        style: TextStyle(
                            fontSize: 18,
                            color: ColorConstant.textMainColor,
                            fontWeight: FontWeight.w800)),
                    SizedBox(height: 10),
                    Text('How far are you willing to travel?',
                        style: TextStyle(
                            fontSize: 14,
                            color: Colors.grey,
                            fontWeight: FontWeight.w200)),
                    CupertinoSlider(
                        min: 0,
                        max: 100,
                        value: this.serviceRadius,
                        onChanged: (value) {
                          setState(() {
                            serviceRadius = value;
                          });
                        }),
                    labelTextFieldWidget('Price per km', pricePerKmController),
                    SizedBox(height: 30),
                    Row(crossAxisAlignment:CrossAxisAlignment.center,children: <Widget>[
                      Text('Extended Service Radius (optional)',
                          style: TextStyle(
                              fontSize: 18,
                              color: ColorConstant.textMainColor,
                              fontWeight: FontWeight.w800)),
                      Spacer(),
                      CupertinoSwitch(activeColor: ColorConstant.themeColor,value: isExtendedService, onChanged: (value) {
                        setState(() {
                          isExtendedService = value;
                        });
                      }),
                    ]),
                    SizedBox(height: 10),
                    Container(
                      color: isExtendedService ? Colors.white : Colors.grey.withOpacity(0.1),
                      child: isExtendedService ? extendedServiceViews() : AbsorbPointer(child: extendedServiceViews()),
                    ),
                    SizedBox(height: 10),
                    Container(
                        height: 2,
                        color: ColorConstant.disableAndTextFieldBGColor),
                    SizedBox(height: 10),
                    labelTextFieldWidget(
                        'Trips per day (max.)', tripsController),
                    SizedBox(height: 10),
                    labelTextFieldWidget(
                        'Free transportation for any order above:',
                        transportController),
                    SizedBox(height: 10),
                    Container(
                        margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          //mainAxisAlignment: MainAxisAlignment.start,
                          //crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Text('Set this as an add on service',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: ColorConstant.textMainColor,
                                      fontWeight: FontWeight.w800)),
                            ),
                            CupertinoSwitch(activeColor: ColorConstant.themeColor,value: isAddon, onChanged: (value) {
                              setState(() {
                                isAddon = value;
                              });
                            })
                          ],
                        ))
                  ]),
                )),
                Container(
                    child: ButtonWidget(
                      onPressed: () {

                          if (serviceRadius == 0.0){

                            showToast('Please select service radius');
                          }else if(pricePerKmController.text.isEmpty == true || int.parse(pricePerKmController.text) == 0){

                            showToast('Please enter price per km');
                          }else if(isExtendedService && (kmOverController.text.isEmpty == true || int.parse(kmOverController.text) == 0)){

                            showToast('Please enter every km over');
                          }else if(tripsController.text.isEmpty == true || int.parse(tripsController.text) == 0){

                            showToast('Please enter trips per day');
                          }else if(transportController.text.isEmpty == true || int.parse(transportController.text) == 0){

                            showToast('Please enter free transportation');
                          }else{

                            if (serviceId != 0){

                              callAPIEditServices();
                            }else{

                              callAPIAddServices();
                            }

                          }

                      },
                      title: "Next",
                      bgColor: ColorConstant.themeColor,
                      textColor: Colors.white,
                      borderColor: Colors.transparent,
                    ),
                    height: 60,
                    width: MediaQuery.of(context).size.width),
              ]),
            )),
      ),
    );
  }
  
  extendedServiceViews(){
    
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Do you want to charge more for further distance?',
            style: TextStyle(
                fontSize: 14,
                color: Colors.grey,
                fontWeight: FontWeight.w200)),
        Container(
          width: MediaQuery.of(context).size.width,
          child: CupertinoSlider(
              min: 0,
              max: 100,
              value: this.extendedServiceRadius,
              onChanged: (value) {
                setState(() {
                  extendedServiceRadius = value;
                });
              }),
        ),
        labelTextFieldWidget('For every km over', kmOverController)  
      ],
    );
  }

  labelTextFieldWidget(String label, TextEditingController controller) {
    return Container(
        margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
        height: 60,
        width: MediaQuery.of(context).size.width,
        child: Row(
          //mainAxisAlignment: MainAxisAlignment.start,
          //crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Text(label,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontSize: 16,
                      color: ColorConstant.textMainColor,
                      fontWeight: FontWeight.w800)),
            ),
            //Spacer(),
            Container(
              margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
              height: 50,
              width: 200,
              child: TextFormField(
                  controller: controller,
                  keyboardType: TextInputType.number,
                  style: TextStyle(
                      fontSize: 14.0, color: ColorConstant.themeColor),
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: ColorConstant.disableAndTextFieldBGColor,
                      hintStyle: TextStyle(
                          fontSize: 14.0, color: ColorConstant.themeColor),
                      hintText: '0',
                      border: InputBorder.none)),
            )
          ],
        ));
  }

  getTransportService() async {
    UserModel userModel = await getCurrentUser();
    int businessId = userModel.businessProfile.id;

    APIManger.shared.makeRequest(
        context: context,
        endPoint: APIConstant.getAddEditTransportService + '/' + '$businessId',
        method: RequestType.GET,
        params: {'status': 'active'},
        callback: (result) {
          if (result is SuccessState) {

            Map<String,dynamic> dict = result.value['payload'];

            if (dict != null){

              setState(() {

                serviceId = dict['id'] ?? 0;

                int price_per_km = dict['price_per_km'];
                pricePerKmController.text = price_per_km.toString();

                int serviceRadiusValue = dict['service_radius'];
                serviceRadius = serviceRadiusValue.toDouble();
                extendedServiceMinimum = serviceRadiusValue + 1.0;
                extendedServiceRadius = serviceRadiusValue + 1.0;

                Map<String,dynamic> location = dict['location'];
                Map<String,dynamic> dictConfig = location['location_order_config'];
                int max_transportation_orders = dictConfig['max_transportation_orders'];
                transportController.text = max_transportation_orders.toString();

                isExtendedService = dict['is_extended_service'];
                int extended_service_radius = dict['extended_service_radius'];
                extendedServiceRadius = extended_service_radius.toDouble();

                int extended_service_price_per_km = dict['extended_service_price_per_km'];
                kmOverController.text = extended_service_price_per_km.toString();

                // double free_transportation_above = dict['free_transportation_above'];
                // .text = free_transportation_above.toString();

                isAddon = dict['is_add_on_only'];
              });
            }

          } else if (result is ErrorState) {
            showToast(result.msg);
          }
        });
  }

  callAPIAddServices() async {
    UserModel userModel = await getCurrentUser();
    int businessId = userModel.businessProfile.id;

    Map<String, dynamic> param = {};
    param.addAll({'location_id': businessId});
    param.addAll({'service_radius': serviceRadius.toInt()});
    param.addAll({'price_per_km': double.parse(pricePerKmController.text)});
    param.addAll({'max_transportation_orders': int.parse(transportController.text)});
    param.addAll({'is_extended_service': isExtendedService});
    if (isExtendedService){

      param.addAll({'extended_service_radius': extendedServiceRadius.toInt()});
      param.addAll({'extended_service_price_per_km': double.parse(kmOverController.text)});
    }

    param.addAll({'is_add_on_only': isAddon});
    param.addAll({'free_transportation_storage': tripsController.text});
    param.addAll({'free_transportation_above': transportController.text});

    APIManger.shared.makeRequest(
        context: context,
        endPoint: APIConstant.getAddEditTransportService,
        method: RequestType.POST,
        params: param,
        callback: (result) {
          if (result is SuccessState) {
            String msg = result.value['msg'];
            showToast(msg);

            Navigator.pop(context);
          } else if (result is ErrorState) {
            showToast(result.msg);
          }
        });
  }

  callAPIEditServices() async {
    UserModel userModel = await getCurrentUser();
    int businessId = userModel.businessProfile.id;

    Map<String, dynamic> param = {};
    param.addAll({'location_id': businessId});
    param.addAll({'service_radius': serviceRadius.toInt()});
    param.addAll({'price_per_km': double.parse(pricePerKmController.text)});
    param.addAll({'max_transportation_orders': int.parse(transportController.text)});
    param.addAll({'is_extended_service': isExtendedService});
    if (isExtendedService){

      param.addAll({'extended_service_radius': extendedServiceRadius.toInt()});
      param.addAll({'extended_service_price_per_km': double.parse(kmOverController.text)});
    }

    param.addAll({'is_add_on_only': isAddon});
    param.addAll({'free_transportation_storage': tripsController.text});
    param.addAll({'free_transportation_above': transportController.text});

    APIManger.shared.makeRequest(
        context: context,
        endPoint: APIConstant.getAddEditTransportService + '/' + '$serviceId',
        method: RequestType.PUT,
        params: param,
        callback: (result) {
          if (result is SuccessState) {
            String msg = result.value['msg'];
            showToast(msg);

            Navigator.pop(context);
          } else if (result is ErrorState) {
            showToast(result.msg);
          }
        });
  }
}

