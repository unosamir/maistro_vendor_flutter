import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/CustomWidget/OrderListWidget.dart';
import 'package:maistro_vendor/Utilities/NavigationMethods.dart';

class Orders extends StatefulWidget {
  @override
  _OrdersState createState() => _OrdersState();
}

class _OrdersState extends State<Orders> {

  // TabController _tabController;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    final orderPending = Container(
        child: ListView.builder(
            primary: false,
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: 10,
            itemBuilder: (context,index){
              return InkWell(
                child: OrderListWidget(),
                onTap: (){
                },
              );
            }),
    );

    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            labelColor: ColorConstant.themeColor,
            unselectedLabelColor: ColorConstant.textMainColor,
            indicatorColor: ColorConstant.themeColor,
            tabs: [
              Tab(text: "Pending"),
              Tab(text: "Approved"),
              Tab(text: "Received"),
              Tab(text: "Complete"),
            ],
          ),
          title: Text('Orders',style: TextStyle(
              fontSize: 26,
              color: ColorConstant.textMainColor,
              fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
          centerTitle: false,
          brightness: Brightness.light,
        ),
        body: TabBarView(
          children: [
            orderPending,
            Center(child: Text("Page 2")),
            Center(child: Text("Page 2")),
            Center(child: Text("Page 2"))
          ],
        ),
      ),
    );
  }
}
