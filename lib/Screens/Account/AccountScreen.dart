
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/CustomWidget/AccountListView.dart';
import 'package:maistro_vendor/Helper/ApplicationData.dart';
import 'package:maistro_vendor/Model/UserModel.dart';
import 'package:maistro_vendor/Utilities/NavigationMethods.dart';
import 'package:maistro_vendor/Utilities/Utilities.dart';

class AccountScreen extends StatefulWidget{

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen>{

  List<String> arrList = ['Services & Prices','Hours & Availability', 'Payment Processing', 'Earnings & Reports', 'About', 'Help & Support', 'Privacy Policy', 'Settings'];
  String bName = '';
  String address = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    setUserData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 20),
            Padding(
              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: Text(bName,
                  style: TextStyle(
                      fontSize: 18,
                      color: ColorConstant.textMainColor,
                      fontWeight: FontWeight.bold)),
            ),
            SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: Text(address,
                  style: TextStyle(
                      fontSize: 14, color: ColorConstant.textMainColor)),
            ),
            SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: GestureDetector(
                onTap: (){

                  navigateToEditAccount(context);
                },
                child: Text('Edit',
                    style: TextStyle(
                        fontSize: 16, color: ColorConstant.themeColor)),
              ),
            ),
            SizedBox(height: 20),
            Container(height: 10,color: ColorConstant.bgColor),
            Expanded(
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: arrList.length,
                  itemBuilder: (context,index){
                    return InkWell(
                      child: AccountListView(arrList[index]),
                      onTap: (){

                        if (index == 0){

                          navigateToServiceList(context);
                        }
                        else if (index == 1){

                          navigateToHoursAvailability(context);
                        }

                      },
                    );
                  }),
            ),
            Container(
              margin: EdgeInsets.only(left: 30, top: 10, right: 30, bottom: 10),
              height: 60,
              width: MediaQuery.of(context).size.width - 60,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30)
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: GestureDetector(
                onTap: (){

                  showAlertDialogWithTwoAction(context, "Sign Out",
                      "Are you sure want to sign out?", "No", "Yes", () {
                        logoutUser(context);
                      });
                },
                child: Center(child: Text('Sign Out',style: TextStyle(
                    fontSize: 16,fontWeight: FontWeight.w500, color: ColorConstant.themeColor))),
              )
            ),
          ],
        ),
      ),
    );
  }

   setUserData() async {

     UserModel userModel = await getCurrentUser();
     setState(() {

       bName = userModel.businessProfile.name;
       address = userModel.businessProfile.address;
     });

  }

}