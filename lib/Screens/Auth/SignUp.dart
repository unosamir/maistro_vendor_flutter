import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/CustomWidget/TextFieldWidget.dart';
import 'package:maistro_vendor/CustomWidget/ButtonWidget.dart';
import 'package:maistro_vendor/Helper/APIManager.dart';
import 'package:maistro_vendor/Utilities/Utilities.dart';

class SignUp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SignUp();
}

class _SignUp extends State<SignUp> {
  TextEditingController fNameCont = TextEditingController();
  TextEditingController lNameCont = TextEditingController();
  TextEditingController emailCont = TextEditingController();
  TextEditingController pwdCont = TextEditingController();
  TextEditingController rePwdCont = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          backgroundColor: Colors.white,
          brightness: Brightness.light,
          elevation: 0,
          iconTheme: IconThemeData(
            color: ColorConstant.textMainColor, //change your color here
          )),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Sign Up',
                    style: TextStyle(
                        fontSize: 30,
                        color: ColorConstant.textMainColor,
                        fontWeight: FontWeight.bold)),
                SizedBox(height: 10),
                Text('Please sign up to get started.',
                    style: TextStyle(
                        fontSize: 14, color: ColorConstant.textMainColor)),
                SizedBox(height: 40),
                TextFieldWidget(
                    controller: fNameCont,
                    placeHolder: 'First Name',
                    isSecure: false,
                    errorMsg: 'Please enter first name'),
                SizedBox(height: 20),
                TextFieldWidget(
                    controller: lNameCont,
                    placeHolder: 'Last Name',
                    isSecure: false,
                    errorMsg: 'Please enter last name'),
                SizedBox(height: 20),
                TextFieldWidget(
                    keyboardType: TextInputType.emailAddress,
                    controller: emailCont,
                    placeHolder: 'Email',
                    isSecure: false,
                    errorMsg: 'Please enter valid email',
                    textFieldType: TextFieldType.EMAIL),
                SizedBox(height: 20),
                TextFieldWidget(
                    controller: pwdCont,
                    placeHolder: 'Password',
                    isSecure: true,
                    errorMsg: 'Please enter valid password',
                    textFieldType: TextFieldType.PASSWORD),
                SizedBox(height: 20),
                TextFieldWidget(
                    controller: rePwdCont,
                    placeHolder: 'Re-enter Password',
                    isSecure: true,
                    errorMsg: 'Please enter valid re enter password',
                    textFieldType: TextFieldType.PASSWORD),
                SizedBox(height: 80),
                Container(
                    child: ButtonWidget(
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          if (pwdCont.text != rePwdCont.text) {
                            showToast(
                                "password and confirm password should be same");
                          } else {
                            callRegisterAPI();
                          }
                        }
                      },
                      title: "Sign Up",
                      bgColor: ColorConstant.themeColor,
                      textColor: Colors.white,
                      borderColor: Colors.transparent,
                    ),
                    height: 60,
                    width: MediaQuery.of(context).size.width - 40),
                SizedBox(height: 20)
              ],
            ),
          ),
        ),
      )),
    );
  }

  callRegisterAPI() {
    APIManger.shared.makeRequest(
        context: context,
        endPoint: APIConstant.registerUser,
        method: RequestType.POST,
        params: {
          "first_name": fNameCont.text,
          "last_name": lNameCont.text,
          "email": emailCont.text,
          "password": pwdCont.text,
          "confirm_password": rePwdCont.text
        },
        callback: (result) {
          if (result is SuccessState) {

            showAlertDialogWithTwoAction(
                context,
                "Verify Email",
                "Please check your email to verify your account.",
                'Cancel',
                'OK', () {

              Navigator.of(context).pop();
            });
          } else if (result is ErrorState) {

            showToast(result.msg);
          }
        });
  }
}
