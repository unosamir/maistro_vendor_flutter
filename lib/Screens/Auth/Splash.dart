import 'dart:async';

import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/CustomWidget/LogoWidget.dart';
import 'package:maistro_vendor/Helper/APIManager.dart';
import 'package:maistro_vendor/Helper/ApplicationData.dart';
import 'package:maistro_vendor/Model/UserModel.dart';
import 'package:maistro_vendor/Utilities/NavigationMethods.dart';
import 'package:maistro_vendor/Utilities/Utilities.dart';

class Splash extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<Splash> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    initialConfig(context);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: ColorConstant.themeColor,
      body: SafeArea(
        child: Container(
          child: LogoWidget(),
        ),
      ),
    );
  }

  initialConfig(BuildContext context) async{

    UserModel userModel = await getCurrentUser();
    if (userModel != null && userModel.token != null){

      callAPIForVerifyLogin(userModel);
    }else{

      navigateToSelectLoginSignup(context);
    }
  }

  callAPIForVerifyLogin(UserModel userModel) {

    APIManger.shared.makeRequest(
        context: context,
        endPoint: APIConstant.verifyLogin,
        method: RequestType.POST,
        params: {
          "email": userModel.email,
        },
        callback: (result) {
           if (result is SuccessState) {

            Map<String, dynamic> payload = result.value['payload'];
            saveLoginDataAndRedirect(context, payload);
          } else if (result is ErrorState) {

            showToast(result.msg);
          }
        });
  }
}
