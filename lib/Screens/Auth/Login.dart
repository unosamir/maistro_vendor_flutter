import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/CustomWidget/TextFieldWidget.dart';
import 'package:maistro_vendor/CustomWidget/ButtonWidget.dart';
import 'package:flutter/services.dart';
import 'package:maistro_vendor/Helper/APIManager.dart';
import 'package:maistro_vendor/Model/UserModel.dart';
import 'package:maistro_vendor/Utilities/NavigationMethods.dart';
import 'package:maistro_vendor/Utilities/Utilities.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Login();
}

class _Login extends State<Login> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          backgroundColor: Colors.white,
          brightness: Brightness.light,
          elevation: 0,
          iconTheme: IconThemeData(
            color: ColorConstant.textMainColor, //change your color here
          )),
      body: SafeArea(
        child: SingleChildScrollView(
            child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Login',
                    style: TextStyle(
                        fontSize: 30,
                        color: ColorConstant.textMainColor,
                        fontWeight: FontWeight.bold)),
                SizedBox(height: 10),
                Text('Please login into your existing account.',
                    style: TextStyle(
                        fontSize: 14, color: ColorConstant.textMainColor)),
                SizedBox(height: 40),
                TextFieldWidget(
                    controller: emailController,
                    placeHolder: 'Email',
                    isSecure: false,
                    keyboardType: TextInputType.emailAddress,
                    errorMsg: 'Please enter valid email',
                    textFieldType: TextFieldType.EMAIL),
                SizedBox(height: 20),
                TextFieldWidget(
                    controller: passwordController,
                    placeHolder: 'Password',
                    isSecure: true,
                    errorMsg: 'Please enter valid password'),
                SizedBox(height: 40),
                GestureDetector(
                  onTap: () {
                    print("forgot password");
                  },
                  child: Center(
                    child: Text('Forgot Password',
                        style: TextStyle(
                            fontSize: 16,
                            color: ColorConstant.themeColor,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
                SizedBox(height: 40),
                Container(
                    child: ButtonWidget(
                      onPressed: () {
                        if (this._formKey.currentState.validate()) {
                          callLoginAPI();
                        }
                      },
                      title: "Login",
                      bgColor: ColorConstant.themeColor,
                      textColor: Colors.white,
                      borderColor: Colors.transparent,
                    ),
                    height: 60,
                    width: MediaQuery.of(context).size.width - 40),
              ],
            ),
          ),
        )),
      ),
    );
  }

  callLoginAPI() {

    APIManger.shared.makeRequest(
        context: context,
        endPoint: APIConstant.loginUser,
        method: RequestType.POST,
        params: {
          "email": emailController.text,
          "password": passwordController.text
        },
        callback: (result) {
          if (result is SuccessState) {

            Map<String, dynamic> payload = result.value['payload'];
            saveLoginDataAndRedirect(context, payload);
          } else if (result is ErrorState) {

            showToast(result.msg);
          }
        });
  }
}
