import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_places_picker/google_places_picker.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/CustomWidget/BusinessProfileServiceBox.dart';
import 'package:maistro_vendor/CustomWidget/ButtonWidget.dart';
import 'package:maistro_vendor/CustomWidget/TextFieldWidget.dart';
import 'package:maistro_vendor/Helper/APIManager.dart';
import 'package:maistro_vendor/Helper/ApplicationData.dart';
import 'package:maistro_vendor/Model/UserModel.dart';
import 'package:maistro_vendor/Utilities/NavigationMethods.dart';
import 'package:maistro_vendor/Utilities/Utilities.dart';

class BusinessProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<BusinessProfileScreen> {
  bool isStorageService = false;
  bool isMaintenanceService = false;
  bool isTransportService = false;

  FocusNode _addressFocus = new FocusNode();
  TextEditingController bNameController = TextEditingController();
  TextEditingController lNumberController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController taxController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController provinceController = TextEditingController();
  TextEditingController postalController = TextEditingController();

  Place place;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    PluginGooglePlacePicker.initialize(
      androidApiKey: "YOUR_ANDROID_API_KEY",
      iosApiKey: "AIzaSyBJs2f0TNNui_OcRHgVRTIsJAvGQ0EB7oA",
    );

    _addressFocus.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    FocusScope.of(context).unfocus();
    _showAutocomplete();
  }

  _showAutocomplete() async {
    var country = "CA";
    var place = await PluginGooglePlacePicker.showAutocomplete(
        mode: PlaceAutocompleteMode.MODE_OVERLAY,
        countryCode: country,
        typeFilter: TypeFilter.ESTABLISHMENT);

    if (!mounted) return;

    this.place = place;
    getFullAddress(place);
  }

  getFullAddress(Place place) async {
    List<Placemark> placemark = await Geolocator()
        .placemarkFromCoordinates(place.latitude, place.longitude);

    setState(() {
      if (placemark != null || placemark.length > 0) {
        var result = placemark[0];
        streetController.text = place.address;
        cityController.text = result.locality;
        provinceController.text = result.country;
        postalController.text = result.postalCode;
      }
    });
  }

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('Business Profile',
              style: TextStyle(
                  fontSize: 18,
                  color: ColorConstant.textMainColor,
                  fontWeight: FontWeight.w800)),
          backgroundColor: Colors.white,
          elevation: 0,
          brightness: Brightness.light,
          actions: <Widget>[
            TextButton(
              onPressed: () {
                showAlertDialogWithTwoAction(context, "Sign Out",
                    "Are you sure want to sign out?", "No", "Yes", () {
                  logoutUser(context);
                });
              },
              child: Text('Sign Out',
                  style: TextStyle(
                      fontSize: 16, color: ColorConstant.textMainColor)),
            ),
          ],
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Form(
                key: _formKey,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Which services(s) do you offer?',
                          style: TextStyle(
                              fontSize: 16,
                              color: ColorConstant.textMainColor)),
                      SizedBox(height: 20),
                      BusinessProfileServiceBox(
                          (isStorage, isMaintenance, isTransport) {
                        isStorageService = isStorage;
                        isMaintenanceService = isMaintenance;
                        isTransportService = isTransport;
                      }),
                      SizedBox(height: 20),
                      Text('Set up your business profile:',
                          style: TextStyle(
                              fontSize: 16,
                              color: ColorConstant.textMainColor)),
                      SizedBox(height: 20),
                      TextFieldWidget(
                          controller: bNameController,
                          placeHolder: 'Business Name',
                          isSecure: false,
                          errorMsg: 'Please enter valid email'),
                      SizedBox(height: 20),
                      TextFieldWidget(
                          controller: lNumberController,
                          placeHolder: 'Business License Number',
                          isSecure: false,
                          errorMsg: 'Please enter license number'),
                      SizedBox(height: 20),
                      TextFieldWidget(
                          controller: phoneController,
                          placeHolder: 'Phone',
                          isSecure: false,
                          keyboardType: TextInputType.number,
                          errorMsg: 'Please enter phone',
                          textFieldType: TextFieldType.PHONE,
                          maxLength: 10),
                      SizedBox(height: 20),
                      TextFieldWidget(
                          controller: taxController,
                          placeHolder: 'Tax',
                          isSecure: false,
                          keyboardType:
                              TextInputType.numberWithOptions(decimal: true),
                          errorMsg: 'Please enter tax'),
                      SizedBox(height: 20),
                      TextFieldWidget(
                          controller: streetController,
                          placeHolder: 'Street Address',
                          isSecure: false,
                          errorMsg: 'Please enter address',
                          textFieldFocus: this._addressFocus),
                      SizedBox(height: 20),
                      Row(
                        children: <Widget>[
                          Expanded(
                              child: TextFieldWidget(
                                  controller: cityController,
                                  placeHolder: 'City',
                                  isSecure: false,
                                  errorMsg: 'Please enter city')),
                          SizedBox(width: 20),
                          Expanded(
                              child: TextFieldWidget(
                                  controller: provinceController,
                                  placeHolder: 'Province',
                                  isSecure: false,
                                  errorMsg: 'Please enter province'))
                        ],
                      ),
                      SizedBox(height: 20),
                      TextFieldWidget(
                          controller: postalController,
                          placeHolder: 'Postal Code',
                          isSecure: false,
                          errorMsg: 'Please enter postal code'),
                      SizedBox(height: 20)
                    ],
                  ),
                )),
          ),
        ),
        bottomNavigationBar: SafeArea(
          child: Container(
              child: ButtonWidget(
                onPressed: () {
                  var list = [
                    isStorageService,
                    isMaintenanceService,
                    isTransportService
                  ];
                  if (list.contains(true)) {
                    if (this._formKey.currentState.validate()) {
                      callBusinessProfileAPI();
                    }
                  } else {
                    showToast('Please select at least one service');
                  }
                },
                title: "NEXT",
                bgColor: ColorConstant.themeColor,
                textColor: Colors.white,
                borderColor: Colors.transparent,
              ),
              height: 60,
              width: MediaQuery.of(context).size.width),
        ));
  }

  callBusinessProfileAPI() async {
    UserModel userModel = await getCurrentUser();

    var param = Map<String, dynamic>();
    param.addAll({"first_name": userModel.first_name});
    param.addAll({"last_name": userModel.last_name});
    param.addAll({"name": this.bNameController.text});
    param.addAll({"license_number": this.lNumberController.text});
    param.addAll({"phone": this.phoneController.text});
    param.addAll({"address": this.streetController.text});
    param.addAll({"city": this.cityController.text});
    param.addAll({"province": this.provinceController.text});
    param.addAll({"is_storage": this.isStorageService});
    param.addAll({"is_maintenance": this.isMaintenanceService});
    param.addAll({"is_transport": this.isTransportService});
    param.addAll({"latitude": this.place.latitude});
    param.addAll({"longitude": this.place.longitude});
    param.addAll({"postal_code": this.postalController.text});
    param.addAll({"tax": double.parse(this.taxController.text)});

    APIManger.shared.makeRequest(
        context: context,
        endPoint: APIConstant.setProfile,
        method: RequestType.POST,
        params: param,
        callback: (result) {
          if (result is SuccessState) {

            Map<String, dynamic> payload = result.value['payload'];
            saveBusinessProfileAndRedirect(context, payload);

          } else if (result is ErrorState) {
            showToast(result.msg);
          }
        });
  }
}
