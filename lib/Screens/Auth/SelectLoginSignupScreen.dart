import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/CustomWidget/ButtonWidget.dart';
import 'package:maistro_vendor/CustomWidget/LogoWidget.dart';
import 'package:maistro_vendor/Utilities/NavigationMethods.dart';

class SelectLoginSignupScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      backgroundColor: ColorConstant.themeColor,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 40),
            Container(child: LogoWidget(),height: 100),
            SizedBox(height: 30),
            Image.asset('assets/images/pattern.png', height: 200,width: MediaQuery.of(context).size.width),
            Container(child: ButtonWidget(
              onPressed: (){

                navigateToSignUp(context);

              },title: "Sign Up",bgColor: Colors.white,textColor: ColorConstant.themeColor,borderColor: ColorConstant.themeColor,
            ),height: 60,width: MediaQuery.of(context).size.width - 40),
            SizedBox(height: 16),
            Container(child: ButtonWidget(
              onPressed: (){

                navigateToLogin(context);

              },title: "Login",bgColor: ColorConstant.themeColor,textColor: Colors.white,borderColor: Colors.white,
            ),height: 60,width: MediaQuery.of(context).size.width - 40),
            Spacer(),
            Container(
              width: 200,
              child: Text('By signing up, you agree to our Terms and Privacy Policy.',
                  maxLines: 2,style: TextStyle(color: Colors.white,fontSize: 14,fontWeight: FontWeight.w100),textAlign: TextAlign.center),
            ),
            SizedBox(height: 10)
          ],
        ),
      ),
    );
  }
}
