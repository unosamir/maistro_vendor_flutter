import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/CustomWidget/UpcomingDataWidget.dart';
import 'package:maistro_vendor/Model/UpcomingDataModel.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    initialConfig();
  }

  List<UpcomingDataModel> arrUpcoming;

  String currentDate;

  @override
  Widget build(BuildContext context) {
    final firstCard = Card(
        child: Row(
      children: <Widget>[
        Expanded(
          child: Column(
            children: <Widget>[
              SizedBox(height: 5),
              Text('0',
                  style:
                      TextStyle(fontSize: 14, color: ColorConstant.themeColor)),
              SizedBox(height: 5),
              Text('Received Today'),
              SizedBox(height: 5)
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: <Widget>[
              SizedBox(height: 5),
              Text('0',
                  style:
                      TextStyle(fontSize: 14, color: ColorConstant.themeColor)),
              SizedBox(height: 5),
              Text('Complete Today'),
              SizedBox(height: 5)
            ],
          ),
        )
      ],
    ));

    final secondCard = Card(
        child: Row(
      children: <Widget>[
        Expanded(
          child: Column(
            children: <Widget>[
              SizedBox(height: 5),
              Text('41',
                  style:
                      TextStyle(fontSize: 14, color: ColorConstant.themeColor)),
              SizedBox(height: 5),
              Text('Pending'),
              SizedBox(height: 5)
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: <Widget>[
              SizedBox(height: 5),
              Text('0',
                  style:
                      TextStyle(fontSize: 14, color: ColorConstant.themeColor)),
              SizedBox(height: 5),
              Text('Approved'),
              SizedBox(height: 5)
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: <Widget>[
              SizedBox(height: 5),
              Text('0',
                  style:
                      TextStyle(fontSize: 14, color: ColorConstant.themeColor)),
              SizedBox(height: 5),
              Text('Received'),
              SizedBox(height: 5)
            ],
          ),
        )
      ],
    ));

    final upcomingCard = Card(
      child: Container(
        child: ListView.builder(
            primary: false,
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: arrUpcoming.length,
            itemBuilder: (context, index) {
              return UpcomingDataWidget(
                  day: arrUpcoming[index].day,
                  receive: arrUpcoming[index].receive,
                  complete: arrUpcoming[index].complete,
                  isHeader: (index == 0) ? true : false);
            }),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Image.asset('assets/images/headerCard.jpg',
                  width: MediaQuery.of(context).size.width),
              Padding(
                padding: EdgeInsets.fromLTRB(16, 50, 0, 6),
                child: Text(this.currentDate,
                    style: TextStyle(
                        fontSize: 28,
                        color: Colors.white,
                        fontWeight: FontWeight.bold)),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(6, 110, 6, 6),
                child: Container(
                  height: MediaQuery.of(context).size.height -
                      160 -
                      MediaQuery.of(context).padding.top,
                  child: ListView(
                    scrollDirection: Axis.vertical,
                    children: <Widget>[
                      firstCard,
                      SizedBox(height: 10),
                      Text('  All Active Orders',
                          style: TextStyle(
                              fontSize: 16,
                              color: ColorConstant.textMainColor,
                              fontWeight: FontWeight.bold)),
                      SizedBox(height: 10),
                      secondCard,
                      SizedBox(height: 10),
                      Text('  Upcoming This Week',
                          style: TextStyle(
                              fontSize: 16,
                              color: ColorConstant.textMainColor,
                              fontWeight: FontWeight.bold)),
                      SizedBox(height: 10),
                      upcomingCard
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  initialConfig() {
    var now = new DateTime.now();
    var formatter = new DateFormat('EEEE\nMMM d, yyyy');

    setState(() {
      currentDate = formatter.format(now);

      arrUpcoming = [];
      arrUpcoming.add(UpcomingDataModel(
          day: 'DAY', receive: 'RECEIVE', complete: 'COMPLETE'));

      for (var i = 0; i < 10; i++) {
        arrUpcoming.add(
            UpcomingDataModel(day: 'Tomorrow', receive: '0', complete: '0'));
      }
    });
  }
}
