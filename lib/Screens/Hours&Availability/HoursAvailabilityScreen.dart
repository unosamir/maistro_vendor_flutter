import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_section_table_view/flutter_section_table_view.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/Constant/EnumConstant.dart';
import 'package:maistro_vendor/Constant/FontConstant.dart';
import 'package:maistro_vendor/CustomWidget/ButtonWidget.dart';
import 'package:maistro_vendor/CustomWidget/GeneralHourWidget.dart';
import 'package:maistro_vendor/Model/CellModel.dart';
import 'package:maistro_vendor/Utilities/Utilities.dart';

class HoursAvailabilityScreen extends StatefulWidget{

  @override
  _HoursAvailabilityScreenState createState() => _HoursAvailabilityScreenState();
}

class _HoursAvailabilityScreenState extends State<HoursAvailabilityScreen> {

  List<SectionModel> arrAvailability = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    prepareDataSource();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CupertinoPageScaffold(
      child: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            CupertinoSliverNavigationBar(
              actionsForegroundColor: Colors.black,
              border: Border.all(color: Colors.transparent),
              backgroundColor: Colors.white,
              brightness: Brightness.light,
              largeTitle: Text('Hours & Availability',
                  style: TextStyle(
                      fontFamily: FontConstant.kFontFamily,
                      fontSize: 22,
                      color: ColorConstant.textMainColor,
                      fontWeight: FontWeight.w800)),
            )
          ];
        },
        body: Material(
            color: Colors.white,
            child: SafeArea(
              child: Column(children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                  alignment: Alignment.centerLeft,
                  child: Text('When are your service available?',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontSize: 16, color: ColorConstant.textMainColor)),
                ),
                SizedBox(height: 20),
                Expanded(child: Padding(
                  padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                  child: SectionTableView(
                    isPrimary: false,
                    sectionCount: arrAvailability.length,
                    numOfRowInSection: (section) {
                      return arrAvailability[section].dataArr.length;
                    },
                    cellAtIndexPath: (section, row) {

                      if (section == 0){

                        return GeneralHourWidget(model: arrAvailability[section].dataArr[row]);
                      }else{

                        return Text('test');
                      }
                    },
                    headerInSection: (section) {

                      if (section == 0){

                        return Padding(
                          padding: EdgeInsets.fromLTRB(0, 5, 0, 10),
                          child: Text('General Hours',
                              style: TextStyle(
                                  fontFamily: FontConstant.kFontFamily,
                                  fontSize: 20,
                                  color: ColorConstant.tabBarTextColor,
                                  fontWeight: FontWeight.w500)),
                        );
                      }else{
                        return Padding(
                          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                          child: Row(
                            children: <Widget>[
                              Text('Exceptions & Holidays',
                                  style: TextStyle(
                                      fontFamily: FontConstant.kFontFamily,
                                      fontSize: 20,
                                      color: ColorConstant.tabBarTextColor,
                                      fontWeight: FontWeight.w500)),
                              Spacer(),
                              GestureDetector(
                                onTap: (){},
                                child: Text('+ Add Date(s)',
                                    style: TextStyle(
                                        fontSize: 16, color: ColorConstant.themeColor)),
                              )
                            ],
                          ),
                        );
                      }
                    },
                  )
                )),
                Container(
                    child: ButtonWidget(
                      onPressed: () {},
                      title: "Save",
                      bgColor: ColorConstant.themeColor,
                      textColor: Colors.white,
                      borderColor: Colors.transparent,
                    ),
                    height: 60,
                    width: MediaQuery.of(context).size.width)
              ]),
            )),
      ),
    );
  }

  prepareDataSource(){

    this.arrAvailability.clear();

    var sectionGen = SectionModel();
    sectionGen.sectionType = CellType.AvailabilityGeneralSection;
    sectionGen.dataArr.add(getCellModel('Sunday'));
    sectionGen.dataArr.add(getCellModel('Monday'));
    sectionGen.dataArr.add(getCellModel('Tuesday'));
    sectionGen.dataArr.add(getCellModel('Wednesday'));
    sectionGen.dataArr.add(getCellModel('Thursday'));
    sectionGen.dataArr.add(getCellModel('Friday'));
    sectionGen.dataArr.add(getCellModel('Saturday'));
    arrAvailability.add(sectionGen);

    var sectionException = SectionModel();
    sectionException.sectionType = CellType.AvailabilityExceptionSection;
    arrAvailability.add(sectionException);

    setState(() {
      arrAvailability = arrAvailability;
    });
  }
}