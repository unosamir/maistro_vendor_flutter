
class ServiceModel {
  int createdBy;
  String createdAt;
  int id;
  int itemId;
  int itemcategoryId;
  String itemcategorycreatedAt;
  String itemcategorydescription;
  int itemcategoryid;
  String itemcategoryimage;
  String itemcategoryname;
  int itemcategorysort;
  String itemcategoryupdatedAt;
  String itemcreatedBy;
  String itemcreatedAt;
  int itemid;
  String itemname;
  int itemsort;
  String itemupdatedBy;
  String itemupdatedAt;
  int locationId;
  String locationaddress;
  int locationadminId;
  int locationavgRating;
  String locationcity;
  String locationcreatedAt;
  int locationid;
  int locationisMaintenance;
  int locationisStorage;
  int locationisTransport;
  double locationlatitude;
  String locationlicenseNumber;
  String locationlocationOrderConfigcreatedBy;
  String locationlocationOrderConfigcreatedAt;
  int locationlocationOrderConfigid;
  int locationlocationOrderConfiglocationId;
  int locationlocationOrderConfigmaxMaintenanceOrders;
  int locationlocationOrderConfigmaxStorageOrders;
  int locationlocationOrderConfigmaxTransportationOrders;
  String locationlocationOrderConfigupdatedBy;
  String locationlocationOrderConfigupdatedAt;
  double locationlongitude;
  String locationname;
  String locationphone;
  String locationpostalCode;
  String locationprovince;
  int locationstatus;
  double locationtax;
  String locationtimezone;
  String locationupdatedAt;
  double price;
  String status;
  String title;
  String descriptionField;
  int updatedBy;
  String updatedAt;
  int days_to_complete;

  bool isSelected = false;
  bool isAddOn = false;

  ServiceModel.fromJSON(Map<String, dynamic> parsedJson) {
    this.days_to_complete = parsedJson['days_to_complete'] ?? 0;
    this.title = parsedJson['title'] ?? '';
    this.descriptionField = parsedJson['description'] ?? '';
    this.createdBy = parsedJson['created_by'] ?? '';
    this.createdAt = parsedJson['createdAt'] ?? '';
    this.id = parsedJson['id'];
    this.isAddOn = parsedJson['is_add_on_only'] == 1 ? true : false;
    this.itemId = parsedJson['item_id'];
    this.itemcategoryId = parsedJson['item.category_id'];
    this.itemcategorycreatedAt = parsedJson['item.category.createdAt'] ?? '';
    this.itemcategorydescription = parsedJson['item.category.description'];
    this.itemcategoryid = parsedJson['item.category.id'];
    this.itemcategoryimage = parsedJson['item.category.image'];
    this.itemcategoryname = parsedJson['item.category.name'];
    this.itemcategorysort = parsedJson['item.category.sort'];
    this.itemcategoryupdatedAt = parsedJson['item.category.updatedAt'] ?? '';
    this.itemcreatedBy = parsedJson['item.created_by'] ?? '';
    this.itemcreatedAt = parsedJson['item.createdAt'] ?? '';
    this.itemid = parsedJson['item.id'];
    this.itemname = parsedJson['item.name'];
    this.itemsort = parsedJson['item.sort'];
    this.itemupdatedBy = parsedJson['item.updated_by'] ?? '';
    this.itemupdatedAt = parsedJson['item.updatedAt'] ?? '';
    this.locationId = parsedJson['location_id'];
    this.locationaddress = parsedJson['location.address'];
    this.locationadminId = parsedJson['location.admin_id'];
    this.locationavgRating = parsedJson['location.avg_rating'];
    this.locationcity = parsedJson['location.city'];
    this.locationcreatedAt = parsedJson['location.createdAt'] ?? '';
    this.locationid = parsedJson['location.id'];
    this.locationisMaintenance = parsedJson['location.is_maintenance'];
    this.locationisStorage = parsedJson['location.is_storage'];
    this.locationisTransport = parsedJson['location.is_transport'];
    this.locationlatitude = parsedJson['location.latitude'];
    this.locationlicenseNumber = parsedJson['location.license_number'];
    this.locationlocationOrderConfigcreatedBy = parsedJson['location.location_order_config.created_by'] ?? '';
    this.locationlocationOrderConfigcreatedAt = parsedJson['location.location_order_config.createdAt'] ?? '';
    this.locationlocationOrderConfigid = parsedJson['location.location_order_config.id'];
    this.locationlocationOrderConfiglocationId = parsedJson['location.location_order_config.location_id'];
    this.locationlocationOrderConfigmaxMaintenanceOrders = parsedJson['location.location_order_config.max_maintenance_orders'];
    this.locationlocationOrderConfigmaxStorageOrders = parsedJson['location.location_order_config.max_storage_orders'];
    this.locationlocationOrderConfigmaxTransportationOrders = parsedJson['location.location_order_config.max_transportation_orders'];
    this.locationlocationOrderConfigupdatedBy = parsedJson['location.location_order_config.updated_by'] ?? '';
    this.locationlocationOrderConfigupdatedAt = parsedJson['location.location_order_config.updatedAt'] ?? '';
    this.locationlongitude = parsedJson['location.longitude'];
    this.locationname = parsedJson['location.name'];
    this.locationphone = parsedJson['location.phone'];
    this.locationpostalCode = parsedJson['location.postal_code'];
    this.locationprovince = parsedJson['location.province'];
    this.locationstatus = parsedJson['location.status'];
    this.locationtax = parsedJson['location.tax'];
    this.locationtimezone = parsedJson['location.timezone'];
    this.locationupdatedAt = parsedJson['location.updatedAt'] ?? '';

    if (parsedJson['price'] is int){
      int x = parsedJson['price'];
      this.price = x.toDouble();
    }else{

      this.price = parsedJson['price'] ?? 0.0;
    }

    this.status = parsedJson['status'];
    this.updatedBy = parsedJson['updated_by'] ?? '';
    this.updatedAt = parsedJson['updatedAt'] ?? '';
  }
}