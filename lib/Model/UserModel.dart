
// import 'package:json_annotation/json_annotation.dart';
// part 'UserModel.g.dart';

// @JsonSerializable()
class UserModel {
  final int id;
  final String first_name;
  final String last_name;
  final String email;
  final String phone;
  final String token;

  // @JsonKey(defaultValue: false)
  final bool marketing_opt_in;
  BusinessProfileModel businessProfile = BusinessProfileModel();


  UserModel({this.id,this.first_name,this.last_name,this.email,this.phone,this.token,this.marketing_opt_in,this.businessProfile});

  // factory UserModel.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  // Map<String, dynamic> toJson() => _$UserToJson(this);

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      UserModel(
          id: json["id"],
          first_name: json["first_name"],
          last_name: json["last_name"],
          email: json["email"],
          phone: json["phone"],
          token: json["token"],
          marketing_opt_in: json["marketing_opt_in"],
          businessProfile: (json['business_profile'] != null ? BusinessProfileModel.fromJson(json['business_profile']) : null)
      );

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "first_name": this.first_name,
      "last_name": this.last_name,
      "email": this.email,
      "phone": this.phone,
      "token": this.token,
      "marketing_opt_in": this.marketing_opt_in,
      "business_profile": (this.businessProfile != null ? this.businessProfile.toJson() : null)
    };
  }
}

class BusinessProfileModel{

  final int id;
  final int admin_id;
  final String name;
  final String phone;
  final String license_number;
  final String province;
  final String city;
  final String address;
  final String postal_code;
  final double latitude;
  final double longitude;
  final double tax;
  final bool status;
  final bool is_storage;
  final bool is_transport;
  final bool is_maintenance;

  BusinessProfileModel({this.id,this.admin_id,this.name,this.phone,this.license_number,this.province,this.city,
    this.address,this.postal_code,this.latitude,this.longitude,this.tax,this.status,this.is_storage,this.is_transport,this.is_maintenance});

  // factory UserModel.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  // Map<String, dynamic> toJson() => _$UserToJson(this);

  factory BusinessProfileModel.fromJson(Map<String, dynamic> json) =>
      BusinessProfileModel(
          id: json["id"],
          admin_id: json["admin_id"],
          name: json["name"],
          phone: json["phone"],
          license_number: json["license_number"],
          province: json["province"],
          city: json["city"],
          address: json["address"],
          postal_code: json["postal_code"],
          latitude: json["latitude"],
          longitude: json["longitude"],
          tax: json["tax"],
          status: (json["status"] == 1 ? true : false),
          is_storage: json["is_storage"],
          is_transport: json["is_transport"],
          is_maintenance: json["is_maintenance"]
      );

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "admin_id": this.admin_id,
      "name": this.name,
      "phone": this.phone,
      "license_number": this.license_number,
      "province": this.province,
      "city": this.city,
      "address": this.address,
      "postal_code": this.postal_code,
      "latitude": this.latitude,
      "longitude": this.longitude,
      "tax": this.tax,
      "status": (this.status == true ? 1 : 0),
      "is_storage": this.is_storage,
      "is_transport": this.is_transport,
      "is_maintenance": this.is_maintenance
    };
  }

}