
class CommonServiceModel{

   String createdBy;
   String createdAt;
   int id;
   String name;
   int sort;
   String updatedBy;
   String updatedAt;
   double price;
   int itemId;
   String title;
   String desc;
   List<CommonServiceModel> items;

  bool isSelected = false;
  bool isAddOn = false;
  bool isAlreadyAdded = false;

  CommonServiceModel({this.createdBy,this.createdAt,this.id,this.name,this.sort,this.updatedBy,this.updatedAt,this.price,this.itemId,this.title,this.desc,this.items});

  factory CommonServiceModel.fromJson(dynamic json) {

    List<CommonServiceModel> arrItems = [];
    if (json['items'] != null) {

      List itemsJSON = json['items'] as List;
      arrItems = itemsJSON.map((item) => CommonServiceModel.fromJson(item)).toList();
    }


    return CommonServiceModel(
        createdBy: json["createdBy"],
        createdAt: json["createdAt"],
        id: json["id"],
        name: json["name"],
        sort: json["sort"],
        updatedBy: json["updatedBy"],
        updatedAt: json["updatedAt"],
        price: json["price"],
        itemId: json["item_id"],
        title: json["title"],
        desc: json["desc"],
        items: arrItems
    );
  }

}

