import 'package:flutter/cupertino.dart';
import 'package:maistro_vendor/Constant/EnumConstant.dart';

String DEFAULT_OPEN_TIME = "09:00:00";
String DEFAULT_CLOSE_TIME = "20:00:00";

class SectionModel{

  String userText;
  CellType sectionType = CellType.none;
  List dataArr = [];
  bool isShowButton;
  String buttonTitle;
}

class CellModel{

  String placeholder;
  String userText;
  CellType cellType = CellType.none;
  dynamic cellObj;
  String imageName;
  Image image;
  bool isSelected = false;
  bool isPromocode;
  List dataArr = [];
  String openTime = DEFAULT_OPEN_TIME;
  String closeTime = DEFAULT_CLOSE_TIME;
  int id;

  bool isShowButton;
  String buttonTitle;
}
