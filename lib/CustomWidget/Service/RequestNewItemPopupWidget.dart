import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/Helper/APIManager.dart';
import 'package:maistro_vendor/Utilities/Utilities.dart';

import '../ButtonWidget.dart';
import '../TextFieldWidget.dart';

showRequestNewItemPopup(BuildContext context) {

  bool _fromTop = true;
  final _formKey = GlobalKey<FormState>();
  bool isSuccess = false;
  TextEditingController itemNameController = TextEditingController();

  showGeneralDialog(
    barrierLabel: "id",
    barrierDismissible: true,
    barrierColor: Colors.black.withOpacity(0.5),
    transitionDuration: Duration(milliseconds: 700),
    context: context,
    pageBuilder: (context, anim1, anim2) {
      return StatefulBuilder(
          builder: (context, setState){
            return Align(
              alignment: _fromTop ? Alignment.topCenter : Alignment.bottomCenter,
              child: Container(
                height: 300,
                color: Colors.white,
                width: MediaQuery.of(context).size.width - 40,
                margin: EdgeInsets.only(top: 100, left: 12, right: 12, bottom: 50),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: 50,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Text('Request an Item',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    decoration: TextDecoration.none,
                                    fontSize: 18,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500)),
                          ),
                          GestureDetector(
                              child: Icon(Icons.close),
                              onTap: () {
                                Navigator.pop(context);
                              }),
                          SizedBox(width: 10)
                        ],
                      ),
                    ),
                    isSuccess ? SizedBox(height: 0):
                    Padding(
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: Text(
                          'Help us improve your experience by suggesting any items that should be serviced with MaiStro.',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              fontSize: 14,
                              color: ColorConstant.textMainColor,
                              fontWeight: FontWeight.normal)),
                    ),
                    SizedBox(height: 20),
                    isSuccess ?
                    Padding(
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: Text(
                          "Thanks for your feedback!\nWe will review your item request.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              fontSize: 14,
                              color: ColorConstant.textMainColor,
                              fontWeight: FontWeight.normal)),
                    ):
                    Padding(
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        child: Material(
                          child: Form(
                            key: _formKey,
                            child: TextFieldWidget(
                                controller: itemNameController,
                                placeHolder: 'Item Name',
                                isSecure: false,
                                keyboardType: TextInputType.name,
                                errorMsg: 'Please enter item name'),
                          ),
                        )),
                    Spacer(),
                    Container(
                        child: ButtonWidget(
                          onPressed: () {

                            if (isSuccess){

                              Navigator.pop(context);
                            }else{

                              if (_formKey.currentState.validate()){

                                //API Call
                                Map<String, dynamic> param = {};

                                param.addAll({'name': itemNameController.text});
                                param.addAll({'status': 0});
                                param.addAll({'sort': 0});

                                APIManger.shared.makeRequest(
                                    context: context,
                                    endPoint: APIConstant.requestNewItem,
                                    method: RequestType.POST,
                                    params: param,
                                    callback: (result) {
                                      if (result is SuccessState) {

                                        setState(() {
                                          isSuccess = true;
                                        });
                                      } else if (result is ErrorState) {
                                        showToast(result.msg);
                                      }
                                    });

                              }
                            }
                          },
                          title: (isSuccess ? "Okay" : "Submit"),
                          bgColor: ColorConstant.themeColor,
                          textColor: Colors.white,
                          borderColor: Colors.transparent,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width)
                  ],
                ),
              ),
            );
          }
      );
    },
    transitionBuilder: (context, anim1, anim2, child) {
      return SlideTransition(
        position:
        Tween(begin: Offset(0, _fromTop ? -1 : 1), end: Offset(0, 0))
            .animate(anim1),
        child: child,
      );
    },
  );
}