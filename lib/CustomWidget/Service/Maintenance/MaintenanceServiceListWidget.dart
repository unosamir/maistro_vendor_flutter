import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/Model/Maintenance/MaintenanceServiceModel.dart';
import 'package:maistro_vendor/Model/ServiceModel.dart';

class MaintenanceServiceListWidget extends StatefulWidget{
  MaintenanceServiceListWidget(
      {this.model,this.onEditTap});
  final MaintenanceItemModel model;
  final Function(ServiceModel) onEditTap;

  @override
  _MaintenanceServiceListState createState() => _MaintenanceServiceListState();
}

class _MaintenanceServiceListState extends State<MaintenanceServiceListWidget> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Padding(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: Card(
        child: Column(
          children: <Widget>[
            GestureDetector(
              onTap: (){
                setState(() {

                  widget.model.isExpand = !widget.model.isExpand;
                });
              },
              child: Container(
                height: 50,
                color: ColorConstant.disableAndTextFieldBGColor,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(width: 10),
                      Text(widget.model.item_name,
                          style: TextStyle(
                              fontSize: 14,
                              color: ColorConstant.textMainColor,
                              fontWeight: FontWeight.bold)),
                      Spacer(),
                      Image.asset(
                          widget.model.isExpand
                              ? 'assets/images/upArrowLine.png'
                              : 'assets/images/downArrowLine.png',
                          height: 18,
                          width: 18),
                      SizedBox(width: 10)
                    ],
                  )
              ),
            ),
           ListView.builder(
                  primary: false,
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: widget.model.isExpand ? widget.model.arrService.length : 0,
                  itemBuilder: (context,index){
                    return MaintenanceListInnerWidget(model: widget.model.arrService[index],onEditTap: (model){
                      widget.onEditTap(model);
                    });
                  }),
          ],
        ),
      ),
    );
  }
}

class MaintenanceListInnerWidget extends StatelessWidget{
  MaintenanceListInnerWidget({@required this.model,this.onEditTap});
  final ServiceModel model;
  final Function(ServiceModel) onEditTap;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(width: 10),
            Text(model.title,
                style: TextStyle(
                    fontSize: 14,
                    color: ColorConstant.textMainColor,
                    fontWeight: FontWeight.w500)),
            Spacer(),
            Text(model.price.toString(),
                style: TextStyle(
                    fontSize: 14,
                    color: ColorConstant.textMainColor,
                    fontWeight: FontWeight.normal)),
            SizedBox(width: 10),
            GestureDetector(
              onTap: (){

                this.onEditTap(model);
              },
              child: Text('Edit',
                  style: TextStyle(
                      fontSize: 16, color: ColorConstant.themeColor)),
            ),
            SizedBox(width: 10),
          ],
        ),
        SizedBox(height: 5),
        Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
          child: Text(model.descriptionField,
              style: TextStyle(
                  fontSize: 12,
                  color: Colors.grey,
                  fontWeight: FontWeight.normal)),
        ),
      ],
    );
  }
}