import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/Model/CommonServiceModel.dart';

class MaintenanceServiceDropDownWidget extends StatefulWidget{
  MaintenanceServiceDropDownWidget({this.initialValueModel,this.mainTitle,this.subTitle,this.dropDownTitle,this.arrData,this.selectedModel});
  final CommonServiceModel initialValueModel;
  final String mainTitle;
  final String subTitle;
  final String dropDownTitle;
  final List<CommonServiceModel> arrData;
  final Function(CommonServiceModel) selectedModel;

  @override
  _MaintenanceServiceDropDownWidgetState createState() => _MaintenanceServiceDropDownWidgetState();
}

class _MaintenanceServiceDropDownWidgetState extends State<MaintenanceServiceDropDownWidget> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  // getDropDownMenuItems() {
  //   // Map<int, dynamic> mapWithIndex = widget.arrData.asMap();
  //    List<DropdownMenuItem> _items = [];
  //   // mapWithIndex.forEach((index, item) {
  //   //   _items.add(
  //   //     DropdownMenuItem<int>(
  //   //       value: index,
  //   //       child: Text(item.name),
  //   //     ),
  //   //   );
  //   // });
  //   // return _items;
  //
  //   widget.arrData.asMap().forEach((index, item) {
  //     if (index == widget.arrData.length - 1){
  //       _items.add(DropdownMenuItem<int>(
  //         value: index,
  //         child: Text('asdasdasdsddasds'),
  //       ));
  //     }else{
  //
  //       _items.add(DropdownMenuItem<int>(
  //                 value: index,
  //                 child: Text(item.name),
  //               ));
  //     }
  //   });
  //
  //   return _items;
  // }

  @override
  Widget build(BuildContext context) {

   final dropdownMenuOptions = widget.arrData
       .map((CommonServiceModel item) => new DropdownMenuItem<CommonServiceModel>(value: item, child: new Text(item.name))).toList();

    // TODO: implement build
    return Padding(
      padding: EdgeInsets.fromLTRB(16, 10, 16, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(this.widget.mainTitle,
              style: TextStyle(
                  fontSize: 16,
                  color: ColorConstant.textMainColor,
                  fontWeight: FontWeight.w500)),
          SizedBox(height: 8),
          Text(this.widget.subTitle,
              style: TextStyle(
                  fontSize: 16,
                  color: ColorConstant.textMainColor,
                  fontWeight: FontWeight.normal)),
          SizedBox(height: 10),
          Container(
            padding: EdgeInsets.all(10),
            height: 50,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                  bottomLeft: Radius.circular(5),
                  bottomRight: Radius.circular(5)
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 0.8,
                  blurRadius: 1.5,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child: DropdownButton<CommonServiceModel>(
              dropdownColor: Colors.white,
              isExpanded: true,
              underline: SizedBox(height: 0),
              hint:  Text(this.widget.dropDownTitle,style: TextStyle(
                  fontSize: 16,
                  color: ColorConstant.textMainColor,
                  fontWeight: FontWeight.w500)),
              icon: Image.asset('assets/images/downArrowLine.png',height: 15,width: 15),
              items: dropdownMenuOptions,
              onChanged: (CommonServiceModel model) {
                setState(() {
                  widget.selectedModel(model);
                });
              },
              value: widget.initialValueModel,
            ),
          ),
        ],
      ),
    );
  }
}