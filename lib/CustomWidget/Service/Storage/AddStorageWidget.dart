import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/Model/CommonServiceModel.dart';

import '../../TextFieldWidget.dart';

class AddStorageWidget extends StatefulWidget {
  AddStorageWidget(this.model, this.onPressed);
  final CommonServiceModel model;
  final GestureTapCallback onPressed;

  @override
  _AddStorageWidgetState createState() => _AddStorageWidgetState();
}

class _AddStorageWidgetState extends State<AddStorageWidget> {
  bool isAddon = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    isAddon = widget.model.isAddOn;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return widget.model.isAlreadyAdded
        ? AbsorbPointer(child: mainWidget())
        : mainWidget();
  }

  mainWidget() {
    return Card(
      color: widget.model.isAlreadyAdded
          ? Colors.white.withOpacity(0.95)
          : Colors.white,
      margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 10),
          GestureDetector(
            onTap: widget.onPressed,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 10),
                Text(widget.model.name,
                    style: TextStyle(
                        fontSize: 14,
                        color: ColorConstant.textMainColor,
                        fontWeight: FontWeight.bold)),
                Spacer(),
                Image.asset(
                    widget.model.isSelected || widget.model.isAlreadyAdded
                        ? 'assets/images/selectedIcon.png'
                        : 'assets/images/unselectedIcon.png',
                    height: 20,
                    width: 20),
                SizedBox(width: 10)
              ],
            ),
          ),
          SizedBox(height: 10),
          widget.model.isSelected || widget.model.isAlreadyAdded
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(width: 10),
                    Text('Set a Price',
                        style: TextStyle(
                            fontSize: 15,
                            color: ColorConstant.textMainColor,
                            fontWeight: FontWeight.w200)),
                    Spacer(),
                    Container(
                      height: 27,
                      width: 45,
                      child: TextField(
                        onChanged: (text){
                          print(text);
                          widget.model.price = double.parse(text);
                        },
                          style: TextStyle(fontSize: 14.0,color: ColorConstant.themeColor),
                          decoration: InputDecoration(
                              hintStyle: TextStyle(
                                  fontSize: 14.0,
                                  color: ColorConstant.themeColor),
                              hintText: '\$0.00',
                              border: InputBorder.none)),
                    ),
                    Container(
                      height: 20,
                      width: 50,
                      child: Text('/ month',
                          style: TextStyle(
                              fontSize: 14, color: ColorConstant.themeColor)),
                    ),
                    SizedBox(width: 10)
                  ],
                )
              : Container(width: 0, height: 0),
          widget.model.isSelected || widget.model.isAlreadyAdded
              ? SizedBox(height: 10)
              : SizedBox(height: 0),
          widget.model.isSelected || widget.model.isAlreadyAdded
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width: 10),
                    Text('Is this an add on only item?',
                        style: TextStyle(
                            fontSize: 15,
                            color: ColorConstant.textMainColor,
                            fontWeight: FontWeight.w200)),
                    Spacer(),
                    CupertinoSwitch(
                        value: isAddon,
                        onChanged: (bool isOn) {
                          setState(() {
                            isAddon = isOn;
                            widget.model.isAddOn = isOn;
                          });
                        }),
                    SizedBox(width: 10)
                  ],
                )
              : Container(width: 0, height: 0),
          widget.model.isSelected || widget.model.isAlreadyAdded
              ? SizedBox(height: 10)
              : SizedBox(height: 0),
        ],
      ),
    );
  }
}
