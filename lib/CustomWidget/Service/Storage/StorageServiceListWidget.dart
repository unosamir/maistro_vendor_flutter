import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/Helper/APIManager.dart';
import 'package:maistro_vendor/Helper/ApplicationData.dart';
import 'package:maistro_vendor/Model/ServiceModel.dart';
import 'package:maistro_vendor/Model/UserModel.dart';
import 'package:maistro_vendor/Utilities/Utilities.dart';


class StorageServiceListWidget extends StatefulWidget {
  StorageServiceListWidget(
      {this.model, this.onEditClick, this.onDeleteClick, this.onSaveClick});
  final ServiceModel model;
  final GestureTapCallback onEditClick;
  final GestureTapCallback onDeleteClick;
  final GestureTapCallback onSaveClick;

  @override
  _StorageServiceListWidgetState createState() =>
      _StorageServiceListWidgetState();
}

class _StorageServiceListWidgetState extends State<StorageServiceListWidget> {
  bool isAddon = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    isAddon = widget.model.isAddOn;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(width: 10),
              Text(widget.model.itemname,
                  style: TextStyle(
                      fontSize: 14,
                      color: ColorConstant.textMainColor,
                      fontWeight: FontWeight.bold)),
            ],
          ),
          SizedBox(height: 10),
          widget.model.isSelected == false
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(width: 10),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          color: ColorConstant.disableAndTextFieldBGColor),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                        child: Text(widget.model.price.toString() + '/month',
                            style: TextStyle(
                                fontSize: 15, color: ColorConstant.themeColor)),
                      ),
                    ),
                    SizedBox(width: 10),
                    widget.model.isAddOn ? Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          color: ColorConstant.disableAndTextFieldBGColor),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                        child: Text('Add On',
                            style: TextStyle(
                                fontSize: 15, color: ColorConstant.themeColor)),
                      ),
                    ) : Container(height: 0.0,width: 0.0),
                    Spacer(),
                    Container(
                      height: 35,
                      width: 35,
                      child: IconButton(
                        icon: Image.asset('assets/images/editAddressIcon.png'),
                        onPressed: widget.onEditClick,
                      ),
                    ),
                    SizedBox(width: 5)
                  ],
                )
              : Container(width: 0, height: 0),
          widget.model.isSelected
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(width: 10),
                    Text('Set a Price',
                        style: TextStyle(
                            fontSize: 15,
                            color: ColorConstant.textMainColor,
                            fontWeight: FontWeight.w200)),
                    Spacer(),
                    Container(
                      height: 27,
                      width: 45,
                      child: TextFormField(
                          initialValue: widget.model.price.toString(),
                          onChanged: (text) {
                            print(text);
                            widget.model.price = double.parse(text);
                          },
                          style: TextStyle(
                              fontSize: 14.0, color: ColorConstant.themeColor),
                          decoration: InputDecoration(
                              hintStyle: TextStyle(
                                  fontSize: 14.0,
                                  color: ColorConstant.themeColor),
                              hintText: '\$0.00',
                              border: InputBorder.none)),
                    ),
                    Text('/ month',
                        style: TextStyle(
                            fontSize: 14, color: ColorConstant.themeColor)),
                    SizedBox(width: 10)
                  ],
                )
              : Container(width: 0, height: 0),
          widget.model.isSelected ? SizedBox(height: 10) : SizedBox(height: 0),
          widget.model.isSelected
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width: 10),
                    Text('Is this an add on only item?',
                        style: TextStyle(
                            fontSize: 15,
                            color: ColorConstant.textMainColor,
                            fontWeight: FontWeight.w200)),
                    Spacer(),
                    CupertinoSwitch(
                        value: isAddon,
                        onChanged: (bool isOn) {
                          setState(() {
                            isAddon = isOn;
                            widget.model.isAddOn = isOn;
                          });
                        }),
                    SizedBox(width: 10)
                  ],
                )
              : Container(width: 0, height: 0),
          widget.model.isSelected ? SizedBox(height: 10) : SizedBox(height: 0),
          widget.model.isSelected
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                        height: 20,
                        width: 50,
                        alignment: Alignment.center,
                        child: InkWell(
                            onTap: (){

                              showAlertDialogWithTwoAction(context, "MaiStro Vendor",
                                  "Are you sure want to delete?", "No", "Yes", () {

                                     callAPIForDeleteService();
                                  });
                            },
                            child: Text('Delete',
                                style: TextStyle(
                                    fontSize: 14,
                                    color: ColorConstant.themeColor)))),
                    Container(
                        height: 20,
                        width: 50,
                        alignment: Alignment.center,
                        child: InkWell(
                            onTap: (){

                              callAPIAddServices();
                            },
                            child: Text('Save',
                                style: TextStyle(
                                    fontSize: 14,
                                    color: ColorConstant.themeColor,
                                    fontWeight: FontWeight.bold)))),
                    SizedBox(width: 10)
                  ],
                )
              : Container(width: 0, height: 0),
          widget.model.isSelected ? SizedBox(height: 10) : SizedBox(height: 0),
        ],
      ),
    );
  }

  //api call
  callAPIForDeleteService(){

    var url = APIConstant.addEditDeleteStorageService+'/'+widget.model.id.toString();
    APIManger.shared.makeRequest(
        context: context,
        endPoint: url,
        method: RequestType.DELETE,
        callback: (result) {
          if (result is SuccessState) {

            showToast("Service Deleted Successfully.");
            widget.onDeleteClick();

          } else if (result is ErrorState) {
            showToast(result.msg);
          }
        });
  }

  callAPIAddServices() async {
    UserModel userModel = await getCurrentUser();
    int businessId = userModel.businessProfile.id;

    Map<String, dynamic> param = {};
    List<Map<String, dynamic>> arrParam = [];

    Map<String, dynamic> paramMM = {};
    paramMM.addAll({'item_id': widget.model.itemid});
    paramMM.addAll({'price': widget.model.price ?? 0.0});
    paramMM.addAll({'is_add_on_only': widget.model.isAddOn ? 1 : 0});
    paramMM.addAll({'status': 'active'});
    arrParam.add(paramMM);

    param.addAll({'location_id': businessId});
    param.addAll({'items': arrParam});

    APIManger.shared.makeRequest(
        context: context,
        endPoint: APIConstant.addEditDeleteStorageService,
        method: RequestType.POST,
        params: param,
        callback: (result) {
          if (result is SuccessState) {

            showToast("Service Updated Successfully.");
            widget.onSaveClick();

          } else if (result is ErrorState) {
            showToast(result.msg);
          }
        });
  }
}
