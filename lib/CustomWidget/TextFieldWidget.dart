import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/Constant/FontConstant.dart';

enum TextFieldType { EMAIL, PASSWORD, PHONE }

// ignore: must_be_immutable
class TextFieldWidget extends StatefulWidget {
  TextFieldWidget(
      {@required this.controller,
      @required this.placeHolder,
      @required this.isSecure,
      this.keyboardType,
      this.errorMsg,
      this.textFieldFocus,
      this.textFieldType,
      this.maxLength});

  final TextEditingController controller;
  final String placeHolder;
  final bool isSecure;
  final TextInputType keyboardType;
  final String errorMsg;
  final int maxLength;

  FocusNode textFieldFocus = new FocusNode();
  TextFieldType textFieldType;

  @override
  _TextFieldWidgetState createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  final TextStyle style =
      TextStyle(fontFamily: FontConstant.kFontFamily, fontSize: 18.0);

  bool _passwordVisible;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    this._passwordVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardAppearance: Brightness.light,
      maxLength: widget.maxLength,
      keyboardType: widget.keyboardType,
      obscureText: widget.isSecure ? !this._passwordVisible : false,
      style: style,
      controller: this.widget.controller,
      focusNode: widget.textFieldFocus,
      validator: widget.textFieldType == TextFieldType.EMAIL
          ? (text) =>
              EmailValidator.validate(text) ? null : this.widget.errorMsg
          : widget.textFieldType == TextFieldType.PASSWORD
              ? (text) {
                  return validatePassword(text);
                }
              : widget.textFieldType == TextFieldType.PHONE
                  ? (text) {
                      return validatePhone(text);
                    }
                  : (text) {
                      if (text == null || text.isEmpty) {
                        return this.widget.errorMsg;
                      }
                      return null;
                    },
      decoration: InputDecoration(
        hintStyle:
            TextStyle(fontSize: 16.0, color: Colors.grey),
        filled: true,
        fillColor: ColorConstant.disableAndTextFieldBGColor,
        contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
        hintText: this.widget.placeHolder,
        border: InputBorder.none,
        suffixIcon: widget.isSecure
            ? IconButton(
                icon: Icon(
                  // Based on passwordVisible state choose the icon
                  _passwordVisible ? Icons.visibility : Icons.visibility_off,
                  color: Theme.of(context).primaryColorDark,
                ),
                onPressed: () {
                  //update state
                  setState(() {
                    _passwordVisible = !_passwordVisible;
                  });
                },
              )
            : null,
      ),
    );
  }

  String validatePassword(String value) {
    String pattern = r'(^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9]).{8,}$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'Please enter password';
    } else if (!regExp.hasMatch(value)) {
      return 'Please enter valid password';
    }
    return null;
  }

  String validatePhone(String value) {
    String pattern = r'(^([0-9]).{9}$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'Please enter phone number';
    } else if (!regExp.hasMatch(value)) {
      return 'Please enter valid phone number';
    }
    return null;
  }
}
