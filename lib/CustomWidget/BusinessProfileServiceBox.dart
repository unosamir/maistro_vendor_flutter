import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';

class BusinessProfileServiceBox extends StatefulWidget {
  BusinessProfileServiceBox(this.callback);
  final Function(bool, bool, bool) callback;

  @override
  _BusinessProfileServiceBoxState createState() =>
      _BusinessProfileServiceBoxState();
}

class _BusinessProfileServiceBoxState extends State<BusinessProfileServiceBox> {
  bool isStorageSelected = false;
  bool isMaintenanceSelected = false;
  bool isTransportSelected = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      children: <Widget>[
        Expanded(
          child: Card(
            clipBehavior: Clip.antiAlias,
            child: InkWell(
              child: Container(
                color: (isStorageSelected
                    ? ColorConstant.selectedServiceBoxColor
                    : Colors.white),
                height: 180,
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                        (isStorageSelected
                            ? 'assets/images/selectedService.png'
                            : 'assets/images/storageIcon.png'),
                        height: 50,
                        width: 50),
                    SizedBox(height: 30),
                    Text('Storage',
                        style: TextStyle(
                            fontSize: 16,fontWeight: FontWeight.w500, color: (this.isStorageSelected ? ColorConstant.themeColor : ColorConstant.textMainColor))),
                  ],
                )
              ),
              onTap: () {
                setState(() {
                  this.isStorageSelected = !this.isStorageSelected;
                  widget.callback(this.isStorageSelected,
                      this.isMaintenanceSelected, this.isTransportSelected);
                });
              },
            ),
          ),
        ),
        Expanded(
          child: Card(
            clipBehavior: Clip.antiAlias,
            child: InkWell(
              child: Container(
                color: (isMaintenanceSelected
                    ? ColorConstant.selectedServiceBoxColor
                    : Colors.white),
                height: 180,
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                        (isMaintenanceSelected
                            ? 'assets/images/selectedService.png'
                            : 'assets/images/maintenanceIcon.png'),
                        height: 50,
                        width: 50),
                    SizedBox(height: 30),
                    Text('Maintenance',
                        style: TextStyle(
                            fontSize: 16,fontWeight: FontWeight.w500, color: (this.isMaintenanceSelected ? ColorConstant.themeColor : ColorConstant.textMainColor))),
                  ],
                )
              ),
              onTap: () {
                setState(() {
                  this.isMaintenanceSelected = !this.isMaintenanceSelected;
                  widget.callback(this.isStorageSelected,
                      this.isMaintenanceSelected, this.isTransportSelected);
                });
              },
            ),
          ),
        ),
        Expanded(
          child: Card(
            clipBehavior: Clip.antiAlias,
            child: InkWell(
              child: Container(
                color: (isTransportSelected
                    ? ColorConstant.selectedServiceBoxColor
                    : Colors.white),
                height: 180,
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                        (isTransportSelected
                            ? 'assets/images/selectedService.png'
                            : 'assets/images/transportIcon.png'),
                        height: 50,
                        width: 50),
                    SizedBox(height: 30),
                    Text('Transport',
                        style: TextStyle(
                            fontSize: 16,fontWeight: FontWeight.w500, color: (this.isTransportSelected ? ColorConstant.themeColor : ColorConstant.textMainColor))),
                  ],
                ),
              ),
              onTap: () {
                setState(() {
                  this.isTransportSelected = !this.isTransportSelected;
                  widget.callback(this.isStorageSelected,
                      this.isMaintenanceSelected, this.isTransportSelected);
                });
              },
            ),
          ),
        ),
      ],
    );
  }
}
