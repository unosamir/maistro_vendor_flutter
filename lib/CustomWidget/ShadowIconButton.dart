import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';

class ShadowIconButton extends StatelessWidget {
  ShadowIconButton({@required this.onPressed,@required this.title});
  final GestureTapCallback onPressed;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(left: 10, top: 10, right: 0, bottom: 0),
        height: 50,
        width: 180,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 3,
              blurRadius: 5,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: GestureDetector(
          onTap: this.onPressed,
          child: Row(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Image.asset('assets/images/addIcon.png',height: 40,width: 40),
              ),
              Center(child: Text(this.title,style: TextStyle(
                  fontSize: 16,fontWeight: FontWeight.bold, color: ColorConstant.textMainColor)))
            ],
          ),
        )
    );
  }
}
