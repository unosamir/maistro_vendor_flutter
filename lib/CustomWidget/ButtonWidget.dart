import 'package:flutter/material.dart';

class ButtonWidget extends StatelessWidget {
  ButtonWidget({@required this.onPressed,this.title,this.bgColor,this.textColor,this.borderColor});
  final GestureTapCallback onPressed;
  final String title;
  final Color bgColor;
  final Color textColor;
  final Color borderColor;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      fillColor: this.bgColor,
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(this.title,
              maxLines: 1,
              style: TextStyle(color: this.textColor,fontWeight: FontWeight.normal,fontSize: 20),
            ),
          ],
        ),
      ),
      onPressed: onPressed,
        shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(2.0),
        side: BorderSide(color: this.borderColor)
    )
    );
  }
}
