import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/Constant/FontConstant.dart';
import 'package:maistro_vendor/Model/CellModel.dart';

class GeneralHourWidget extends StatefulWidget{
  GeneralHourWidget({@required this.model});
  final CellModel model;
  @override
  _GeneralHourWidgetState createState() => _GeneralHourWidgetState();
}

class _GeneralHourWidgetState extends State<GeneralHourWidget> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(widget.model.placeholder,
                    style: TextStyle(
                        fontFamily: FontConstant.kFontFamily,
                        fontSize: 16,
                        color: ColorConstant.tabBarTextColor,
                        fontWeight: FontWeight.w500)),
                Spacer(),
                CupertinoSwitch(activeColor: ColorConstant.themeColor,value: widget.model.isSelected, onChanged: (value) {
                  setState(() {
                    widget.model.isSelected = value;
                  });
                }),
              ],
            ),
            widget.model.isSelected ? Padding(padding:EdgeInsets.fromLTRB(0, 5, 0, 8),child: Container(height: 1,color: ColorConstant.disableAndTextFieldBGColor)) : Container(),
            widget.model.isSelected ? Row(
              children: <Widget>[
                Text('Hours',
                    style: TextStyle(
                        fontFamily: FontConstant.kFontFamily,
                        fontSize: 14,
                        color: ColorConstant.tabBarTextColor,
                        fontWeight: FontWeight.normal)),
                Spacer(),
                GestureDetector(
                  onTap: (){},
                  child: Text(widget.model.openTime,
                      style: TextStyle(
                          fontSize: 16, color: ColorConstant.textFieldTextColor)),
                ),
                Image.asset('assets/images/downArrowLine.png',height: 10,width: 10),
                SizedBox(width: 5),
                Text('to'),
                SizedBox(width: 5),
                GestureDetector(
                  onTap: (){},
                  child: Text(widget.model.openTime,
                      style: TextStyle(
                          fontSize: 16, color: ColorConstant.textFieldTextColor)),
                ),
                Image.asset('assets/images/downArrowLine.png',height: 10,width: 10),

              ],
            ) : Container()
          ],
        ),
      ),
    );
  }
}