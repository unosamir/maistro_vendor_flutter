import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';

class NoDataFoundWidget extends StatelessWidget {
  NoDataFoundWidget(this.message);
  final String message;
  @override
  Widget build(BuildContext context) {
    return  Container(
      alignment: Alignment.center,
      child:Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset('assets/images/noDataIcon.png',height: 120,width: 120),
          Text(this.message,
              style: TextStyle(
                  fontSize: 16,
                  color: ColorConstant.textMainColor))
        ],
      )
    );
  }
}
