import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';

class OrderListWidget extends StatefulWidget{

  @override
  _OrderListWidgetState createState() => _OrderListWidgetState();
}

class _OrderListWidgetState extends State<OrderListWidget> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Row(
          children: <Widget>[
            Image.asset('assets/images/placeholderImage.jpg',
                width: 80,height: 60,fit: BoxFit.fill),
            Expanded(child: Padding(
              padding: EdgeInsets.fromLTRB(6, 6, 6, 6),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Bike',style: TextStyle(
                      fontSize: 14,
                      color: ColorConstant.textMainColor,
                      fontWeight: FontWeight.bold)),
                  Text('Receive Today',style: TextStyle(
                      fontSize: 14,
                      color: ColorConstant.textMainColor,
                      fontWeight: FontWeight.normal)),
                  Text('Home',style: TextStyle(
                      fontSize: 14,
                      color: ColorConstant.textMainColor,
                      fontWeight: FontWeight.normal))
                ],
              ),
            )),
            Image.asset('assets/images/transportService.png',
                width: 30,height: 30,fit: BoxFit.fill),
            Image.asset('assets/images/transportService.png',
                width: 30,height: 30,fit: BoxFit.fill),
            Image.asset('assets/images/transportService.png',
                width: 30,height: 30,fit: BoxFit.fill)
          ],
        ),
      ),
    );
  }
}