import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';

class AccountListView extends StatefulWidget {
  AccountListView(this.name);
  final String name;
  @override
  _AccountListViewState createState() => _AccountListViewState();
}

class _AccountListViewState extends State<AccountListView> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 15),
          Padding(
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Row(
              children: <Widget>[
                Text(widget.name,
                    style: TextStyle(
                        fontSize: 15,
                        color: ColorConstant.textMainColor,
                        fontWeight: FontWeight.w200)),
                Spacer(),
                Image.asset('assets/images/rightIcon.png',
                    width: 15, height: 15, fit: BoxFit.fitHeight),
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(height: 5, color: ColorConstant.bgColor)
        ],
      ),
    );
  }
}
