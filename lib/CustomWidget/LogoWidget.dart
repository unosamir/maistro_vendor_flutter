import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';

class LogoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: ColorConstant.themeColor,
      body: Center(
          child: Container(
            height: 100,
              width: 300,
              child: Column(
                children: <Widget>[
                  Text('MAISTRO',
                      style: TextStyle(
                          fontSize: 42,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center),
                  Image.asset('assets/images/VENDOR.png',height: 20,width: 160)
                ],
              ))),
    );
  }
}

//
