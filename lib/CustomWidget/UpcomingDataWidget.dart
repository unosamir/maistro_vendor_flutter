import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';

class UpcomingDataWidget extends StatelessWidget{
  UpcomingDataWidget({this.day, this.receive,this.complete,this.isHeader});
  final String day;
  final String receive;
  final String complete;
  final bool isHeader;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        SizedBox(height: 5),
        Row(
          children: <Widget>[
            Expanded(
                child: Text(this.day,
                    style:
                    TextStyle(fontSize: 14, color: ColorConstant.textMainColor),textAlign: TextAlign.center)
            ),
            Expanded(
                child: Text(this.receive,
                    style:
                    TextStyle(fontSize: 14, color: this.isHeader ? ColorConstant.textMainColor : ColorConstant.themeColor),textAlign: TextAlign.center)
            ),
            Expanded(
                child: Text(this.complete,
                    style:
                    TextStyle(fontSize: 14, color: this.isHeader ? ColorConstant.textMainColor : ColorConstant.themeColor),textAlign: TextAlign.center)
            )
          ],
        ),
        SizedBox(height: 5)
      ],
    );
  }
}