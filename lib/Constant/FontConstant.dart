
class FontConstant{

  //FamilyName
  static String kFontFamily    = 'SFProDisplay';

  static String kProDisplayRegularFont    = 'SFProDisplay-Regular';
  static String kProDisplayBoldFont       = 'SFProDisplay-Bold';
  static String kProDisplaySemiBoldFont   = 'SFProDisplay-Semibold';
  static String kProDisplayMediumFont     = 'SFProDisplay-Medium';
}