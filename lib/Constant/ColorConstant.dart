import 'package:flutter/material.dart';
import 'dart:ui' show Color;

class ColorConstant{

  //color
  static Color themeColor   = Color(0xFF2C9FD0);
  static Color textMainColor   = Color(0xFF4A4A4A);
  static Color selectedServiceBoxColor   = Color(0xFFE1EFF3);
  static Color textFieldTextColor   = Color(0xFF222222);
  static Color textFieldErrorColor   = Color(0xFFD0021B);
  static Color disableAndTextFieldBGColor   = Color(0xFFF5F5F5);
  static Color offSwitchColor   = Color(0xFFD3D3D3);
  static Color orderDisableColor   = Color(0xFFCFCFCF);
  static Color tabBarTextColor   = Color(0xFF43484D);
  static Color pendingConfirmation   = Color(0xFFD14300);
  static Color packageDisabled   = Color(0xFFCFD3D6);
  static Color profileServiceBG   = Color(0xFFE1EFF3);
  static Color bgColor   = Color(0xFFF9F9F9);

  static MaterialColor materialThemeColor   = MaterialColor(0xFF2C9FD0,color);

}

Map<int, Color> color =
{
  50:Color.fromRGBO(136,14,79, .1),
  100:Color.fromRGBO(136,14,79, .2),
  200:Color.fromRGBO(136,14,79, .3),
  300:Color.fromRGBO(136,14,79, .4),
  400:Color.fromRGBO(136,14,79, .5),
  500:Color.fromRGBO(136,14,79, .6),
  600:Color.fromRGBO(136,14,79, .7),
  700:Color.fromRGBO(136,14,79, .8),
  800:Color.fromRGBO(136,14,79, .9),
  900:Color.fromRGBO(136,14,79, 1),
};