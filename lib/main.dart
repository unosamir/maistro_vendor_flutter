import 'package:flutter/material.dart';
import 'package:maistro_vendor/Constant/ColorConstant.dart';
import 'package:maistro_vendor/Constant/FontConstant.dart';
import 'package:maistro_vendor/Screens/Auth/Splash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MaiStro Vendor',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: ColorConstant.materialThemeColor,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        fontFamily: FontConstant.kFontFamily,
      ),
      home: Splash(),
    );
  }
}
