import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:maistro_vendor/Helper/ApplicationData.dart';
import 'package:maistro_vendor/Model/UserModel.dart';
import 'package:maistro_vendor/Screens/Auth/BusinessProfileScreen.dart';
import 'package:maistro_vendor/Screens/Account/EditAccount.dart';
import 'package:maistro_vendor/Screens/Auth/Login.dart';
import 'package:maistro_vendor/Screens/Auth/SelectLoginSignupScreen.dart';
import 'package:maistro_vendor/Screens/Hours&Availability/HoursAvailabilityScreen.dart';
import 'package:maistro_vendor/Screens/Service/Maintenance/MaintenanceServiceList.dart';
import 'package:maistro_vendor/Screens/Service/ServiceList.dart';
import 'package:maistro_vendor/Screens/Auth/SignUp.dart';
import 'package:maistro_vendor/Screens/Service/Storage/StorageServiceList.dart';
import 'package:maistro_vendor/Screens/Dashboard/TabBarScreen.dart';
import 'package:maistro_vendor/Screens/Service/Transport/AddTransportService.dart';
import 'package:shared_preferences/shared_preferences.dart';

navigateToSelectLoginSignup(BuildContext context) {
  Navigator.of(context, rootNavigator: true).pushReplacement(
    MaterialPageRoute(builder: (context) => SelectLoginSignupScreen()),
  );
}

navigateToLogin(BuildContext context) {
  Navigator.of(context, rootNavigator: true).push(
    MaterialPageRoute(builder: (context) => Login()),
  );
}

navigateToSignUp(BuildContext context) {
  Navigator.of(context, rootNavigator: true).push(
    MaterialPageRoute(builder: (context) => SignUp()),
  );
}

navigateToDashboard(BuildContext context) {
  Navigator.of(context).pushAndRemoveUntil(
    // the new route
    MaterialPageRoute(
      builder: (BuildContext context) => TabBarScreen(),
    ),

    // this function should return true when we're done removing routes
    // but because we want to remove all other screens, we make it
    // always return false
        (Route route) => false,
  );
}

saveLoginDataAndRedirect(BuildContext context,Map<String,dynamic> payload) async {

  SharedPreferences sharedUser = await SharedPreferences.getInstance();
  String user = jsonEncode(UserModel.fromJson(payload));
  sharedUser.setString('user', user);

  UserModel userModel = await getCurrentUser();
  if (userModel.businessProfile == null){

    //navigateTo business screen
    navigateToBusinessProfile(context);
  }else{

    //login
    navigateToDashboard(context);
  }
}

saveBusinessProfileAndRedirect(BuildContext context,Map<String,dynamic> payload) async{

  UserModel userModel = await getCurrentUser();
  BusinessProfileModel model = BusinessProfileModel.fromJson(payload);
  userModel.businessProfile = model;

  SharedPreferences sharedUser = await SharedPreferences.getInstance();
  String user = jsonEncode(UserModel.fromJson(userModel.toJson()));
  sharedUser.setString('user', user);

}

logoutUser(BuildContext context) async {
  SharedPreferences pref = await SharedPreferences.getInstance();
  pref.clear();
  Navigator.of(context).pushAndRemoveUntil(
    // the new route
    MaterialPageRoute(
      builder: (BuildContext context) => SelectLoginSignupScreen(),
    ),

    // this function should return true when we're done removing routes
    // but because we want to remove all other screens, we make it
    // always return false
        (Route route) => false,
  );
}

navigateToBusinessProfile(BuildContext context) {
  Navigator.of(context).pushAndRemoveUntil(
    // the new route
    MaterialPageRoute(
      builder: (BuildContext context) => BusinessProfileScreen(),
    ),

    // this function should return true when we're done removing routes
    // but because we want to remove all other screens, we make it
    // always return false
        (Route route) => false,
  );
}

navigateToEditAccount(BuildContext context) {
  Navigator.of(context, rootNavigator: true).push(
    MaterialPageRoute(builder: (context) => EditAccount()),
  );
}

navigateToServiceList(BuildContext context) {
  Navigator.of(context, rootNavigator: true).push(
    MaterialPageRoute(builder: (context) => ServiceList()),
  );
}

navigateToHoursAvailability(BuildContext context) {
  Navigator.of(context, rootNavigator: true).push(
    MaterialPageRoute(builder: (context) => HoursAvailabilityScreen()),
  );
}

navigateToStorageServiceList(BuildContext context) {
  Navigator.of(context, rootNavigator: true).push(
    MaterialPageRoute(builder: (context) => StorageServiceList()),
  );
}

navigateToMaintenanceServiceList(BuildContext context) {
  Navigator.of(context, rootNavigator: true).push(
    MaterialPageRoute(builder: (context) => MaintenanceServiceList()),
  );
}

navigateToTransportServiceList(BuildContext context) {
  Navigator.of(context, rootNavigator: true).push(
    MaterialPageRoute(builder: (context) => AddTransportService()),
  );
}
